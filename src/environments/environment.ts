// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment : any = {
  "production": false,
  "APIEndpoint": 'http://localhost:9000/',
  "Hotels" : 'hotels',
  "HotelsApproved" : 'hotels/approved',
  "HotelsSearch" : 'hotels/search/',
  "HotelsList" : 'hotels/list/',
  "Destinations" : 'destinations',
  "TreksLevel": 'treks/level',
  "TravelAgents": 'travel-agent',
  "TravelAgentsApproved": 'travel-agent/approved',
  "TravelAgentslist":'travel-agent/list/',
  "Trek" : 'treks',
  "FeedBack" : 'feedback',
  "DestinationsTicketBooking" : "destinations/ticket-booking/",
  "DestinationsPermit" : "destinations/permit-required/",
  "ExperienceType" : "experience/type/",
  "Experience" : "experience",
  "Masters" : "masters",
  "User" : "user-details",
  "UserByType" : "user-details/type/",
  "UserById" : "user-details/id/",
  "UserReqOtp" : "user-request-otp",
  "UserResetPwd" : "user-reset-pwd/",
  "Order" : "order",
  "Order-Bulk" : "order-bulk",
  "Permit" : "permit",
  "FileUpload" : "file-upload",
  "RazorKey" : "rzp_test_wS3m6RchQD9wqP"

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
