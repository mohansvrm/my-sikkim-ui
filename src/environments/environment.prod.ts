export const environment = {
  production: true,
  "APIEndpoint": 'http://35.165.212.231:9000/',
  "Hotels" : 'hotels',
  "HotelsSearch" : 'hotels/search/',
  "Destinations" : 'destinations',
  "TreksLevel": 'treks/level',
  "TravelAgents": 'travel-agents',
  "Trek" : 'treks',
  "FeedBack" : 'feedback',
  "DestinationsTicketBooking" : "destinations/ticket-booking/",
  "ExperienceType" : "experience/type/",
  "Experience" : "experience",
  "Masters" : "masters",
  "Cart" : "cart"
};
