import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DialogComponent } from './dialog/dialog.component';
import { ConfirmDialogService } from './dialog/dialog.service';



@NgModule({
  declarations: [
    DialogComponent
  ],
  imports: [
    CommonModule
  ],
  exports : [
    DialogComponent
  ],
  providers : [
    ConfirmDialogService
  ]
})
export class SharedModule { }
