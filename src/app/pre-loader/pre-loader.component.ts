import { Component, OnInit } from '@angular/core';
import { LoaderService } from '../core/services/loader.service';

@Component({
  selector: 'app-pre-loader',
  templateUrl: './pre-loader.component.html',
  styleUrls: ['./pre-loader.component.css']
})
export class PreLoaderComponent implements OnInit {

  loading : boolean = false;
  constructor(private loaderService: LoaderService) {

    this.loaderService.isLoading.subscribe((v) => {
      
      this.loading = v;
    });

  }

  ngOnInit(): void {
  }

}
