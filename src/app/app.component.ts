import { Component, OnInit } from '@angular/core';
import { ApiService } from './core/services/api.service';
import { SharedService } from './core/services/shared.service';
import { Response } from './core/models/Response';
import { LoaderService } from './core/services/loader.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent  implements OnInit{
  title = 'my-sikkim-ui';
  loading : boolean = false;
 constructor(   public sharedService : SharedService,
   public apiService: ApiService,
  public loaderService: LoaderService) 
 {
  
 }
 ngOnInit(): void {
  this.apiService.get('Masters').subscribe((result: Response) => {
    if (result.isSuccess) {
      this.sharedService.masters = result.data;        
      
    }
  });

  this.loaderService.isLoading.subscribe((v) => {
      
      this.loading = v;
    });
}

 
}
