import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { OwlModule } from 'ngx-owl-carousel';
import { AppRoutingModule } from './app-routing.module';

// import { SocialLoginModule, SocialAuthServiceConfig } from 'angularx-social-login';
import {GoogleLoginProvider, SocialLoginModule} from 'angularx-social-login';
import { LoaderInterceptor } from './core/services/loader-interceptor.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { LoginOptionComponent } from './login-option/login-option.component';
import { FormsModule } from '@angular/forms';
import { PreLoaderComponent } from './pre-loader/pre-loader.component';
import { UserRegisterComponent } from './user-register/user-register.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { ForgetPasswordComponent } from './forget-password/forget-password.component'; 
import { SharedModule } from './shared/shared.module';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    LoginOptionComponent,
    PreLoaderComponent,
    UserRegisterComponent,
    ResetPasswordComponent,
    ForgetPasswordComponent 
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    OwlModule,
    HttpClientModule,
    FormsModule,
    SocialLoginModule,
    SharedModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true },
    {
      provide: 'SocialAuthServiceConfig',
      useValue: {
        autoLogin: true,
        providers: [
          {
            id: GoogleLoginProvider.PROVIDER_ID,
            provider: new GoogleLoginProvider('523225139201-ots73e2u3pcvi3shpnv9rsr7ntssn5eb.apps.googleusercontent.com')
          }
        ]
      }
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
 