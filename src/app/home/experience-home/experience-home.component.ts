import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
declare var $: any;
@Component({
  selector: 'app-experience-home',
  templateUrl: './experience-home.component.html',
  styleUrls: ['./experience-home.component.css']
})
export class ExperienceHomeComponent implements OnInit {
  @ViewChild('carousel') hero_slides!: ElementRef;
  @ViewChild('collection') collection_slides!: ElementRef;
  constructor() { }

  ngOnInit(): void {
  }


  ngAfterViewInit() {


    $(this.hero_slides.nativeElement).owlCarousel({
      items: 1,
      loop: true,
      autoplay: true,
      dots: true,
      center: true,
      margin: 0,
      animateIn: 'fadeIn',
      animateOut: 'fadeOut'
    });


    $(this.collection_slides.nativeElement).owlCarousel({
      items: 2,
      margin: 16,
      loop: true,
      autoplay: true,
      smartSpeed: 800,
      dots: false,
      nav: false,
      responsive: {
        1400: {
          items: 3,
        },
        992: {
          items: 3,
        },
        768: {
          items: 3,
        },
        360: {
          items: 1,
        },
      },
    });

 
  }
 

}
