import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { UserInfo } from 'src/app/core/models/UserInfo';
import { SharedService } from 'src/app/core/services/shared.service';
declare var $: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  @ViewChild('carousel') hero_slides!: ElementRef;
  @ViewChild('collection') collection_slides!: ElementRef;
  @ViewChild('testimonial') testimonial_style!: ElementRef;
  userInfo : UserInfo = new UserInfo();
  constructor(public sharedService : SharedService,private router: Router) {
    this.sharedService.castUser.subscribe(user => this.userInfo = user);
   }
  ngOnInit(): void {
  }


  ngAfterViewInit() {


    $(this.hero_slides.nativeElement).owlCarousel({
      items: 1,
      loop: true,
      autoplay: true,
      dots: true,
      center: true,
      margin: 0,
      animateIn: 'fadeIn',
      animateOut: 'fadeOut'
    });


    $(this.collection_slides.nativeElement).owlCarousel({
      items: 2,
      margin: 16,
      loop: true,
      autoplay: true,
      smartSpeed: 800,
      dots: false,
      nav: false,
      responsive: {
        1400: {
          items: 3,
        },
        992: {
          items: 3,
        },
        768: {
          items: 3,
        },
        360: {
          items: 1,
        },
      },
    });


    $(this.testimonial_style.nativeElement).owlCarousel({
            items: 1,
            loop: true,
            autoplay: true,
            dots: true,
            center: true,
            margin: 0,
            animateIn: 'fadeIn',
            animateOut: 'fadeOut'
        })
  }

}
