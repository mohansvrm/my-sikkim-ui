import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ApiService } from '../../core/services/api.service';
import {Response } from '../../core/models/Response';
import { UtilityService } from '../../core/services/utility.service';
import { Destination } from 'src/app/core/models/Destination';
import { Images } from 'src/app/core/models/Image';
declare var $: any;
@Component({
  selector: 'app-permit-info',
  templateUrl: './permit-info.component.html',
  styleUrls: ['./permit-info.component.css']
})
export class PermitInfoComponent implements OnInit {
  @ViewChild('carousel') hero_slides!: ElementRef;
  destinations : Destination [] = [];

  constructor(public apiService : ApiService, public utilityService : UtilityService) { }

  ngOnInit(): void {
    this.apiService.getWithPathValue('DestinationsPermit',true).subscribe((result : Response)  => {
      if(result.isSuccess) {
        this.destinations = result.data;
      } 
    });

   
  }

  ngAfterViewInit() {


    $(this.hero_slides.nativeElement).owlCarousel({
      items: 1,
      loop: true,
      autoplay: true,
      dots: true,
      center: true,
      margin: 0,
      animateIn: 'fadeIn',
      animateOut: 'fadeOut'
    });

  }
    
 

   getSrc(dest : Destination) { 
    if(dest.images) {
      const mainImage : Images | undefined = dest.images.find(image => image.isMainImage);
      if(mainImage) {
        return mainImage.path;
      }
    }
    return '';
   }
 

}
