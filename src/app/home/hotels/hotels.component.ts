import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../core/services/api.service';
import {Response } from '../../core/models/Response';
import { UtilityService } from 'src/app/core/services/utility.service';
import { Hotel } from 'src/app/core/models/Hotel';
import { SharedService } from 'src/app/core/services/shared.service';
import { Images } from 'src/app/core/models/Image';


@Component({
  selector: 'app-hotels',
  templateUrl: './hotels.component.html',
  styleUrls: ['./hotels.component.css']
})
export class HotelsComponent implements OnInit {
  searchWord : string = '';
  listOfHotels : Hotel []= [];
  listOfHotels_bk : Hotel []= [];

  constructor(public apiService : ApiService, 
     public utilityService : UtilityService,
     public sharedService : SharedService ) {
       let isCompoInit : boolean = false;
       this.sharedService.filterEvent.subscribe( res => {
          
         if(isCompoInit) {
          this.listOfHotels = this.listOfHotels_bk.filter(hotel => {

           const selectedDistrict = this.sharedService.headerInfo.filters.filter( (obj: { name: string; }) => obj.name === 'District');
            
           if(selectedDistrict.length) {
            const listOfCheckedDist : any []= selectedDistrict[0].list.filter((li: { checked: any; }) =>
             li.checked).map((sel: { code: any; }) => sel.code);
             if(listOfCheckedDist.includes(hotel.district)) {
               return true;
             }
           }
           return false;
          });
         }
         isCompoInit = true;
       })
      }

  ngOnInit(): void {
    this.apiService.get('HotelsApproved').subscribe((result : Response) => {
      if(result.isSuccess) {
        this.listOfHotels = result.data; 
        this.listOfHotels_bk = result.data; 
      }
    })
  }

  onChangeSearch(search : string) {
    if(search.length < 4) {
      return;
    }
    this.apiService.getWithPathValue('HotelsSearch',search).subscribe((result : Response) => {
      if(result.isSuccess) {
        this.listOfHotels = result.data; 
        this.listOfHotels_bk = result.data; 
      }
    })
  }

  getSrc(hotel : Hotel) { 
    if(hotel.images) {
      const mainImage : Images | undefined = hotel.images.find(image => image.isMainImage);
      if(mainImage) {
        return mainImage.path;
      }
    }
    return '';
   }

}
