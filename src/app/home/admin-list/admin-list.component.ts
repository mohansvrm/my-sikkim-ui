import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/core/services/api.service';

import {Response } from 'src/app/core/models/Response';
import { UserInfo } from 'src/app/core/models/UserInfo';
import { SharedService } from 'src/app/core/services/shared.service';
import { Alert } from 'src/app/core/models/Alert';
import { ConfirmDialogService } from 'src/app/shared/dialog/dialog.service';
@Component({
  selector: 'app-admin-list',
  templateUrl: './admin-list.component.html',
  styleUrls: ['./admin-list.component.css']
})
export class AdminListComponent implements OnInit {
  listOfUsers : UserInfo [] = [];
  constructor(public apiService : ApiService, public sharedService : SharedService,
    private confirmDialogService: ConfirmDialogService) { }

  ngOnInit(): void {
    this.apiService.getWithPathValue('UserByType','ADMIN').subscribe((result: Response) => {
      if(result.isSuccess) {
        this.listOfUsers  = result.data;
      }
    });
  }

  onClickDeactive(user : UserInfo) {
    user.isActive = false;
    this.apiService.put('User','/'+user._id, user).subscribe((result: Response) => {
      if (result.isSuccess) {
          this.sharedService.setAlert(new Alert('success','Deactivated successfully',false))
      }
    });
  }

  onClickResetPwd(user : UserInfo) {

     
    this.confirmDialogService.confirmThis("Are you sure to reset password?", () => {
        this.apiService.put('UserResetPwd',user._id, user).subscribe((result: Response) => {
      if (result.isSuccess) {
          this.sharedService.setAlert(new Alert('success','Passsword is reset successfully',false))
      }
    });
    },
    () => {

    })
    
    
    // function () {  
    //   alert("Yes clicked");  
    // }, function () {  
    //   alert("No clicked");  
    // })  

    // this.apiService.put('UserResetPwd','/'+user._id, user).subscribe((result: Response) => {
    //   if (result.isSuccess) {
    //       this.sharedService.setAlert(new Alert('success','Passsword is reset successfully',false))
    //   }
    // });
  }
}
