import { Component,ElementRef, OnInit,ViewChild} from '@angular/core';
import { ApiService } from '../../core/services/api.service';
import {Response } from '../../core/models/Response';
import { SharedService } from 'src/app/core/services/shared.service';
import { UtilityService } from 'src/app/core/services/utility.service';
import { UserInfo } from 'src/app/core/models/UserInfo'; 
import { Alert } from 'src/app/core/models/Alert';
declare var $: any;

@Component({
  selector: 'app-editProfile',
  templateUrl: './editProfile.component.html',
  styleUrls: ['./editProfile.component.css']
})
export class EditProfileComponent implements OnInit {
editprofile :any;
comment: string = '';
rating: number = 0;

user : UserInfo = new UserInfo();
constructor(public apiService: ApiService,   public utilityService: UtilityService,
    public sharedService: SharedService) { }

  ngOnInit(): void {
    this.sharedService.castUser.subscribe(user => this.user = user);
  }
  onSelectFile(event : any, prop : string) {
    this.apiService.upload("FileUpload", event.target.files[0]).subscribe((result: any) => {
      if(result.body.isSuccess) {
        if(prop === 'profile') {
          this.user.profile = result.body.data;
          
          // this.sharedService.setUser( this.user);
        } else if(prop === 'idproof') {
          this.user.idproof = result.body.data;
        } else if(prop === 'vaccine') {
          this.user.vaccine = result.body.data;
        }
        
      }
    })
  }

  onClickSubmit(): void {
     this.apiService.put('User','/'+this.user._id, this.user).subscribe((result: Response) => {
      if (result.isSuccess) {
          this.editprofile = result.data;
          this.sharedService.setUser(result.data);
          this.sharedService.setAlert(new Alert('success','Profile updated successfully',false));
      }
    });
  }
}
