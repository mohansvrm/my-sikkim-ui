import { Component, OnInit,ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Travelagent } from 'src/app/core/models/travelagent';
import { UserInfo } from 'src/app/core/models/UserInfo';
import { InputPattern } from 'src/app/core/pattern/InputPattern';
import { ApiService } from 'src/app/core/services/api.service';
import { SharedService } from 'src/app/core/services/shared.service';
import { UtilityService } from 'src/app/core/services/utility.service';
import {Response } from '../../core/models/Response';

@Component({
  selector: 'app-add-agent',
  templateUrl: './add-agent.component.html',
  styleUrls: ['./add-agent.component.css']
})
export class AddAgentComponent implements OnInit {
  hotelServices : any[] = [];
  listOfGrade : any[] = [];
  agentInfo : Travelagent = new Travelagent();
  //districts : any = [];
  emailPattern : string = InputPattern.EMAIL;
  userInfo : UserInfo = new UserInfo();
  agentId : any;
  action : any;
  confirmationModal: any = {};
  @ViewChild('addagent') addagent!: any;
  constructor(public sharedService : SharedService, 
    public utilityService : UtilityService,
    public apiService : ApiService, private _Activatedroute: ActivatedRoute) {
      this.listOfGrade = this.utilityService.getMasterByLookUpName('Grade');
      this.hotelServices = this.utilityService.getMasterByLookUpName('HotelServices');
      this.sharedService.castUser.subscribe(user => this.userInfo = user);
      this.agentId = this._Activatedroute.snapshot.paramMap.get("id");
      if(this.agentId) {
        this.apiService.getWithPathValue('TravelAgents', '/' + this.agentId).subscribe((result: Response) => {
          if (result.isSuccess) {
            this.agentInfo = result.data;
            if(!this.agentInfo.amenities || !this.agentInfo.amenities.length) {
            
            let ids = new Set(this.agentInfo.amenities.map(d => d.code));
            this.agentInfo.amenities = [...this.agentInfo.amenities, ...this.hotelServices.filter(d => !ids.has(d.code))];
            }
          }
        });
      } else {
        this.agentInfo.amenities = this.utilityService.getMasterByLookUpName('HotelServices');
      }
     }

 
  ngOnInit(): void {
    
   
  }
  ngAfterViewInit() {
    
    
  }


  type : string = '';
  onClickOfOpenComment(type: string) {
    this.type = type;
     
    let doc: any = document.getElementById("comments");
    doc.style.display = "block";
  }

  callBackFromPopup(event : any) {
    this.agentInfo.status =  this.type;
    if(!this.agentInfo.comments) {
      this.agentInfo.comments = [];
    }
    this.agentInfo.comments.push({...event});
    this.apiService.put('TravelAgents','/' +  this.agentInfo._id,this.agentInfo).subscribe(
      (result : Response) => {
      if(result.isSuccess) {
       alert("Successfully updated");
      }
    })
  

  }
  onClickSubmit() {
      if(this.agentId) {
      this.apiService.put('TravelAgents','/' + this.agentId,this.agentInfo).subscribe((result : Response) => {
        if(result.isSuccess) {
         alert("Successfully updated");
        }
      })
    } else {
      // this.agentInfo.agentName = this.userInfo.emailId;
      this.apiService.post('TravelAgents','',this.agentInfo).subscribe((result : Response) => {
        if(result.isSuccess) {
          
          this.agentInfo._id = result.data._id;
          alert("Successfully created");
        }
      })
    }
  
  }

}
