import { Component, ElementRef, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../../core/services/api.service';
import {Response } from '../../core/models/Response';
import { UtilityService } from 'src/app/core/services/utility.service';
declare var $: any;
@Component({
  selector: 'app-experience-details',
  templateUrl: './experience-details.component.html',
  styleUrls: ['./experience-details.component.css']
})
export class ExperienceDetailsComponent implements OnInit {
  @ViewChild('product') product_slides!: ElementRef; 
  @ViewChildren('products')
  products!: QueryList<any>;
  id : string | null = '';
  experience : any;
  constructor(public apiService : ApiService, private _Activatedroute:ActivatedRoute ,
    public utilityService: UtilityService) { 
    this.id=this._Activatedroute.snapshot.paramMap.get("id");
  }
  

  ngOnInit(): void {
    this.apiService.getWithPathValue('Experience','/' + this.id).subscribe((result : Response) => {
      if(result.isSuccess) {
        this.experience = result.data; 
        for (const image of this.experience.images) {
          // 
          image.base = this.utilityService.convertArrayToBase64(image.data.data, image.contentType);
        }
      }
    });
  }

  // ngAfterViewInit() {


  //   $(this.product_slides.nativeElement).owlCarousel({
  //           items: 1,
  //           margin: 0,
  //           loop: false,
  //           autoplay: true,
  //           autoplayTimeout: 5000,
  //           dots: false,
  //           nav: true,
  //           navText: [('<i class="lni lni-chevron-left"></i>'), ('<i class="lni lni-chevron-right"></i>')]
  //       })
  //     }

      ngAfterViewInit() {
        this.products.changes.subscribe(t => {
          this.rederoOwlCarousel();
        })
      }
     
    
      rederoOwlCarousel() {
        $(this.product_slides.nativeElement).owlCarousel({
          items: 1,
          margin: 0,
          loop: false,
          autoplay: true,
          autoplayTimeout: 5000,
          dots: false,
          nav: true,
          navText: [('<i class="lni lni-chevron-left"></i>'), ('<i class="lni lni-chevron-right"></i>')]
      })
      }
    
  }
