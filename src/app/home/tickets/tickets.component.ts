import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../core/services/api.service';
import {Response } from '../../core/models/Response';
import { UtilityService } from '../../core/services/utility.service';
import { Destination } from 'src/app/core/models/Destination';
import { Images } from 'src/app/core/models/Image';

@Component({
  selector: 'app-tickets',
  templateUrl: './tickets.component.html',
  styleUrls: ['./tickets.component.css']
})
export class TicketsComponent implements OnInit {

  destinations : Destination [] = [];

  constructor(public apiService : ApiService, public utilityService : UtilityService) { }

  ngOnInit(): void {
    this.apiService.getWithPathValue('DestinationsTicketBooking',true).subscribe((result : Response)  => {
      if(result.isSuccess) {
        this.destinations = result.data;
      } 
    });

   
  }

  getSrc(dest : Destination) { 
    if(dest.images) {
      const mainImage : Images | undefined = dest.images.find(image => image.isMainImage);
      if(mainImage) {
        return mainImage.path;
      }
    }
    return '';
   }

}
