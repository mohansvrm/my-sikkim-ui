import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from 'src/app/core/services/api.service';
import { SharedService } from 'src/app/core/services/shared.service';
import { UtilityService } from 'src/app/core/services/utility.service';
import { WindowRefService } from '../../core/services/window-ref.service';
import {Response } from '../../core/models/Response';
import { Order } from 'src/app/core/models/Cart';
import { UserInfo } from 'src/app/core/models/UserInfo';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css'],
  providers: [WindowRefService]
})
export class CartComponent implements OnInit {
  listOfCarts : Order[] = [];
  totalAmount : number = 0;
  user : UserInfo = new UserInfo();
  constructor(private winRef: WindowRefService,public apiService : ApiService, private _Activatedroute:ActivatedRoute ,
    public utilityService: UtilityService, public sharedService : SharedService) {
      
    }

  ngOnInit(): void {
    this.sharedService.castUser.subscribe(user => this.user = user);
    this.apiService.getWithPathValue('Order','/Cart/'+this.user.emailId).subscribe((result : Response)  => {
      if(result.isSuccess) {
        if(result.data.length) {
          this.listOfCarts = result.data;
          this.updateCartDetails();
        }
      } 
    });
  }

  onClickOfDelete(cart : Order)  {
    this.apiService.delete('Order','/'+cart._id).subscribe((result : Response)  => {
      if(result.isSuccess) {
        const index = this.listOfCarts.findIndex(selectedCart => selectedCart._id === cart._id);
        this.listOfCarts.splice(index , 1);
        this.updateCartDetails();
      } 
    });
  }

  updateCartDetails() {
        this.sharedService.headerInfo.cartCount = "";
        this.totalAmount = 0;
          for(const cart of this.listOfCarts) {
            const adultFee = cart.adultFee * cart.adultCount; 
            const childrenFee = cart.childrenFee * cart.childrenCount;
            this.totalAmount = this.totalAmount + adultFee + childrenFee;
          }
          this.sharedService.headerInfo.cartCount = this.listOfCarts.length;
  }


  onClickCheckOut() {
    const options: any = {
      key: 'rzp_test_wS3m6RchQD9wqP',
      amount: this.totalAmount, // amount should be in paise format to display Rs 1255 without decimal point
      currency: 'INR',
      name: 'smohan.thi@gmail.com', // 
      order_id: "" // order_id created by you in backend
    };
    options.handler = ((response: any, error:any) => {
      options.response = response;
      this.listOfCarts.forEach(cart => {
        cart.orderId = "";
        cart.type = 'Order';
        cart.razorPaymentId = response.razorpay_payment_id;
      });

      this.apiService.put('Order-Bulk','',this.listOfCarts).subscribe((result : Response) => {
        if(result.isSuccess) {
            
        }
      });
      
      // call your backend api to verify payment signature & capture transaction
    });
  
    const rzp = new this.winRef.nativeWindow.Razorpay(options);
    rzp.open();
  }

}
