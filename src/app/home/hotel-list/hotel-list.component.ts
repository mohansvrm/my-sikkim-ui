import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRouteSnapshot } from '@angular/router';
import { Alert } from 'src/app/core/models/Alert';
import { Hotel } from 'src/app/core/models/Hotel';
import { Images } from 'src/app/core/models/Image';
import { UserInfo } from 'src/app/core/models/UserInfo';
import { ApiService } from 'src/app/core/services/api.service';
import { SharedService } from 'src/app/core/services/shared.service';
import { Response } from '../../core/models/Response';

@Component({
  selector: 'app-hotel-list',
  templateUrl: './hotel-list.component.html',
  styleUrls: ['./hotel-list.component.css']
})
export class HotelListComponent implements OnInit {
  userInfo: UserInfo = new UserInfo();
  listOfHotels: Hotel[] = [];
  confirmationModal: any = {};
  type: string = '';
  selectedHotel : Hotel = new Hotel();
  constructor(public sharedService: SharedService, public apiService: ApiService) {
    this.sharedService.castUser.subscribe(user => this.userInfo = user);


    //
    if (this.userInfo.userType === 'ADMIN') {
      this.apiService.get('Hotels').subscribe((result: Response) => {
        if (result.isSuccess) {
          this.listOfHotels = result.data;
          
        }
      });
    }

    if (this.userInfo.userType === 'BUSINESS') {
      this.apiService.getWithPathValue('HotelsList', this.userInfo.emailId).subscribe((result: Response) => {
        if (result.isSuccess) {
          this.listOfHotels = result.data;
        }
      });
    }

  }

  ngOnInit(): void {
  }


  onClickOfOpenComment(hotel: Hotel, type: string) {
    this.type = type;
    this.selectedHotel = hotel;
    let doc: any = document.getElementById("comments");
    doc.style.display = "block";
  }

  callBackFromPopup(event : any) {
    this.selectedHotel.status =  this.type;
    if(!this.selectedHotel.comments) {
      this.selectedHotel.comments = [];
    }
    this.selectedHotel.comments.push({...event});
    this.apiService.put('Hotels','/' +  this.selectedHotel._id,this.selectedHotel).subscribe(
      (result : Response) => {
      if(result.isSuccess) {
       alert("Successfully updated");
      }
    })
  }

  onClickOfDelete(hotel: Hotel, index: number) {
    hotel.isActive = false;
    this.apiService.put('Hotels', '/' + hotel._id,hotel).subscribe((result: Response) => {
      if (result.isSuccess) {
        this.sharedService.setAlert(new Alert("success","Deactivated successfully",false));
      }

    });
  }

  onClickOfActivate(hotel: Hotel) { 
    hotel.isActive = true;
    hotel.status = 'Initiated';
    this.apiService.put('Hotels', '/' + hotel._id,hotel).subscribe((result: Response) => {
      if (result.isSuccess) {
        this.sharedService.setAlert(new Alert("success","Deactivated successfully",false));
      }

    });
  }

  getSrc(dest : Hotel) { 
    if(dest.images) {
      const mainImage : Images | undefined = dest.images.find(image => image.isMainImage);
      if(mainImage) {
        return mainImage.path;
      }
    }
    return '';
   }


}
