import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../core/services/api.service';
import {Response } from '../../core/models/Response';
import { UtilityService } from '../../core/services/utility.service';
import { SharedService } from 'src/app/core/services/shared.service';
import { UserInfo } from 'src/app/core/models/UserInfo';
import { Destination } from 'src/app/core/models/Destination';
import { Images } from 'src/app/core/models/Image';

@Component({
  selector: 'app-destinations',
  templateUrl: './destinations.component.html',
  styleUrls: ['./destinations.component.css']
})
export class DestinationsComponent implements OnInit {

  destinations : Destination [] = [];
  userInfo : UserInfo = new UserInfo();
  destinations_bk: Destination [] = [];
  constructor(public apiService : ApiService, public utilityService : UtilityService, public sharedService: SharedService)
    { 
      this.sharedService.castUser.subscribe(user => this.userInfo = user);

      let isCompoInit : boolean = false;
      this.sharedService.filterEvent.subscribe( res => {
         
        if(isCompoInit) {
         this.destinations = this.destinations_bk.filter(destination => {

          const selectedDistrict = this.sharedService.headerInfo.filters.filter( (obj: { name: string; }) => obj.name === 'District');
           
          if(selectedDistrict.length) {
           const listOfCheckedDist : any []= selectedDistrict[0].list.filter((li: { checked: any; }) =>
            li.checked).map((sel: { code: any; }) => sel.code);
            if(listOfCheckedDist.includes(destination.district)) {
              return true;
            }
          }
          return false;
         });
        }
        isCompoInit = true;
      })
    }

  ngOnInit(): void {
    this.apiService.get('Destinations').subscribe((result : Response)  => {
      if(result.isSuccess) {
        this.destinations = result.data;
      if (this.userInfo.userType==='FOREIGNER'){
        this.destinations = this.destinations.filter(dest =>dest.isForeigner);
      }
      this.destinations_bk  = this.destinations ;

      } 
    });
  }

  getSrc(dest : Destination) { 
    if(dest.images) {
      const mainImage : Images | undefined = dest.images.find(image => image.isMainImage);
      if(mainImage) {
        return mainImage.path;
      }
    }
    return '';
   }


}
