import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { HomeComponent } from './home/home.component';
import { HomeRoutingModule } from './home-routing.module';
import { MainComponent } from './main/main.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { HotelsComponent } from './hotels/hotels.component';
import { DestinationsComponent } from './destinations/destinations.component'
import { DestinationComponent } from './destination/destination.component'
import { TreksComponent } from './treks/treks.component';
import { TrekComponent } from './trek/trek.component';
import { HotelComponent } from './hotel/hotel.component';
import { FormsModule } from '@angular/forms';
import { ExperienceComponent } from './experience/experience.component';
import { TravelAgentComponent } from './travel-agent/travel-agent.component';
// import { AdventuresComponent } from './adventures/adventures.component';
import { ExperienceDetailsComponent } from './experience-details/experience-details.component';
import { CuisinesComponent } from './cuisines/cuisines.component';
import { TicketsComponent } from './tickets/tickets.component';
import { FeedbackComponent } from './feedback/feedback.component';
import { ExperienceHomeComponent } from './experience-home/experience-home.component';
import { ProfileComponent } from './profile/profile.component';
import { EditProfileComponent } from './editProfile/editProfile.component';
import { HotelFormComponent } from './hotel-form/hotel-form.component';
import { CartComponent } from './cart/cart.component';
import { PermitInfoComponent } from './permit-info/permit-info.component';
import { ApplyPermitComponent } from './apply-permit/apply-permit.component';
import { BookTicketComponent } from './book-ticket/book-ticket.component';
import { TicketTemplateComponent } from './ticket-template/ticket-template.component';
import { HotelListComponent } from './hotel-list/hotel-list.component';
import { AddAgentComponent } from './add-agent/add-agent.component';
import { MyOrdersComponent } from './my-orders/my-orders.component';
import { CommentsPopupComponent } from './comments-popup/comments-popup.component';
import { DestinationListComponent } from './destination-list/destination-list.component';
import { DestinationFormComponent } from './destination-form/destination-form.component';
import { TravelagentListComponent } from './agent-list/agent-list.component';
import { AdminListComponent } from './admin-list/admin-list.component';
import { AdminFormComponent } from './admin-form/admin-form.component';
import { AlertComponent } from './alert/alert.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [
    HomeComponent,
    MainComponent,
    HeaderComponent,
    FooterComponent,
    HotelsComponent,
    DestinationsComponent,
    TreksComponent,
    HotelComponent,
    ExperienceComponent,
    DestinationComponent,
    TravelAgentComponent, 
    TrekComponent,
    CuisinesComponent, 
    TicketsComponent,
    FeedbackComponent,
    ExperienceHomeComponent,
    ExperienceDetailsComponent,
    HotelFormComponent,
    AddAgentComponent,
    TravelAgentComponent,
    ProfileComponent,
    EditProfileComponent,
    CartComponent,
    ProfileComponent,
    EditProfileComponent,
    PermitInfoComponent,
    ApplyPermitComponent,
    BookTicketComponent,
    TicketTemplateComponent,
    HotelListComponent,
    MyOrdersComponent,
    CommentsPopupComponent,
    DestinationListComponent,
    DestinationFormComponent,
    TravelagentListComponent,
    AdminListComponent,
    AdminFormComponent,
    AlertComponent

  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ]
})
export class HomeModule { }
