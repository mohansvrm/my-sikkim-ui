import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../core/services/api.service';
import {Response } from '../../core/models/Response';


@Component({
  selector: 'app-travel-agent',
  templateUrl: './travel-agent.component.html',
  styleUrls: ['./travel-agent.component.css']
})
export class TravelAgentComponent implements OnInit {

  listOfAgents : any = [];

  constructor(public apiService : ApiService) { }
 

  ngOnInit(): void {
    this.apiService.get('TravelAgentsApproved').subscribe((result : Response) => {
      if(result.isSuccess) {
        this.listOfAgents = result.data; 
      }
    })
  }

}
