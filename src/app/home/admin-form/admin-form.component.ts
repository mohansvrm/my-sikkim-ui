import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SocialAuthService } from 'angularx-social-login';
import { UserInfo } from 'src/app/core/models/UserInfo';
import { InputPattern } from 'src/app/core/pattern/InputPattern';
import { ApiService } from 'src/app/core/services/api.service';
import { SharedService } from 'src/app/core/services/shared.service';
import { UtilityService } from 'src/app/core/services/utility.service';
import {Response } from 'src/app/core/models/Response';
import { Alert } from 'src/app/core/models/Alert';
import { ConfirmDialogService } from 'src/app/shared/dialog/dialog.service';
@Component({
  selector: 'app-admin-form',
  templateUrl: './admin-form.component.html',
  styleUrls: ['./admin-form.component.css']
})
export class AdminFormComponent implements OnInit {
  userId : any;
  action : string;
  userInfo : UserInfo = new UserInfo();
  confirmPassword : string = '';
  type : any;
  emailPattern : string = InputPattern.EMAIL;
  phoneNoPatter : string = InputPattern.PHONE_NUMBER; 
  pwdPattern : string = InputPattern.PASSWORD;
  viewMode : boolean = false;
  roles : any = [];
  constructor(private router: Router,private socialAuthService: SocialAuthService, 
    public sharedService : SharedService,private confirmDialogService: ConfirmDialogService,
    public apiService: ApiService,  public utilityService: UtilityService,
    private _Activatedroute:ActivatedRoute) { 
      this.action  = this._Activatedroute.snapshot.data['path'];
      console.log(this.action);
      this.roles = this.utilityService.getMasterByLookUpName('Roles');
      this.viewMode = this.action === 'view-admin' ? true : false;
      // this.sharedService.castUser.subscribe(user => this.userInfo = user);
      this.userId = this._Activatedroute.snapshot.paramMap.get("id");
      if(this.userId) {
        this.apiService.getWithPathValue('UserById',this.userId).subscribe((result: Response) => {
          if(result.isSuccess) {
            this.userInfo = result.data;
          }
        })
      }
    }

  ngOnInit(): void {
  }
 

  onClickOfSubmit() {
    if(this.userInfo.userName && this.userInfo.phoneNumber &&
      this.userInfo.emailId ) {

      this.confirmDialogService.confirmThis("Once you created, you cannot modify the details. Are you ok to procced?", () => {
        this.userInfo.userType = 'ADMIN';
        this.apiService.post("User", "/Register", this.userInfo).subscribe((result: Response) => {
          if (result.isSuccess) {
            this.viewMode = true;
            this.sharedService.setAlert(new Alert('success', 'Admin Successfully Created', false));
          } else {
            this.sharedService.setAlert(new Alert('danger', result.data, false))
          }
        });
      },
        () => {

        })


        
      } else {
        this.sharedService.setAlert(new Alert('danger',"Fill all mandatory details",false))
      }
    
  }

}
