import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRouteSnapshot } from '@angular/router';
import { Hotel } from 'src/app/core/models/Hotel';
import { Travelagent } from 'src/app/core/models/travelagent';
import { UserInfo } from 'src/app/core/models/UserInfo';
import { ApiService } from 'src/app/core/services/api.service';
import { SharedService } from 'src/app/core/services/shared.service';
import {Response } from 'src/app/core/models/Response';

@Component({
  selector: 'app-agent-list',
  templateUrl: './agent-list.component.html',
  styleUrls: ['./agent-list.component.css']
})
export class TravelagentListComponent implements OnInit {
  userInfo : UserInfo = new UserInfo();
  listOfTravelagent : Travelagent [] = [];
  selecteagent : Travelagent = new Travelagent();
  type: string = '';

  constructor( public sharedService: SharedService, public apiService : ApiService) { 
    this.sharedService.castUser.subscribe(user => this.userInfo = user);

    
    //
    if(this.userInfo.userType === 'ADMIN') {
      this.apiService.get('TravelAgents').subscribe((result: Response) => {
        if(result.isSuccess) {
          this.listOfTravelagent  = result.data;
          
        }
      });
    }

    if(this.userInfo.userType === 'BUSINESS') {
      this.apiService.getWithPathValue('TravelAgentslist', this.userInfo.emailId ).subscribe((result: Response) => {
        if(result.isSuccess) {
          this.listOfTravelagent  = result.data;
        }
      });
    }
   
  }

  ngOnInit(): void {
  }

  onClickOfOpenComment(travelagent : Travelagent, type: string) {
    this.type = type;
    this.selecteagent = travelagent;
    let doc: any = document.getElementById("comments");
    doc.style.display = "block";
  }

  callBackFromPopup(event : any) {
    this.selecteagent.status =  this.type;
    if(!this.selecteagent.comments) {
      this.selecteagent.comments = [];
    }
    this.selecteagent.comments.push({...event});
    this.apiService.put('Travelagent','/' +  this.selecteagent._id,this.selecteagent).subscribe(
      (result : Response) => {
      if(result.isSuccess) {
       alert("Successfully updated");
      }
    })
  }

  onClickOfReject(travelagent : Travelagent, index : number) {
  
    // this.apiService.delete('Hotels', '/' + hotel._id ).subscribe((result: Response) => {
    //   if(result.isSuccess) {
    //     this.listOfHotels.splice(index , 1);
    //   }
      
    // });
  }

  onClickOfDelete(travelagent : Travelagent, index : number) {
  
    this.apiService.delete('Travelagent', '/' + travelagent._id ).subscribe((result: Response) => {
      if(result.isSuccess) {
        this.listOfTravelagent.splice(index , 1);
      }
      
    });
  }

}
