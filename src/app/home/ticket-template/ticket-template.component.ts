import { Component, Input, OnInit } from '@angular/core';
import { Order } from 'src/app/core/models/Cart';
import { jsPDF } from 'jspdf';
import html2canvas from 'html2canvas';

@Component({
  selector: 'app-ticket-template',
  templateUrl: './ticket-template.component.html',
  styleUrls: ['./ticket-template.component.css']
})
export class TicketTemplateComponent implements OnInit {
  @Input() orders : Order[] = [];
  constructor() { }

  ngOnInit(): void {
  }

  getDate(order : Order) {
    return new Date(order.orderDate).getDate();
  }

  getMonth(order : Order) {
    return new Date(order.orderDate).toLocaleString('default', { month: 'short' });
  }

  getYear(order : Order) {
    return new Date(order.orderDate).getFullYear();
  }

  onClickDownload(elementId : string) {
    const data : any = document.getElementById(elementId);
    html2canvas(data).then(canvas => {
      // Few necessary setting options
      var imgWidth = 235;
      var pageHeight = 200;
      var imgHeight = canvas.height * imgWidth / canvas.width;
      var heightLeft = imgHeight;

      const contentDataURL = canvas.toDataURL('image/png')
      let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF
      var position = 0;
      pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)
      pdf.save(elementId + '.pdf'); // Generated PDF
    });
  }
}
