import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Destination } from 'src/app/core/models/Destination';
import { Images } from 'src/app/core/models/Image';
import { UserInfo } from 'src/app/core/models/UserInfo';
import { InputPattern } from 'src/app/core/pattern/InputPattern';
import { ApiService } from 'src/app/core/services/api.service';
import { SharedService } from 'src/app/core/services/shared.service';
import { UtilityService } from 'src/app/core/services/utility.service';
import {Response } from '../../core/models/Response';

@Component({
  selector: 'app-destination-form',
  templateUrl: './destination-form.component.html',
  styleUrls: ['./destination-form.component.css']
})
export class DestinationFormComponent implements OnInit {
  districts : any = [];
  emailPattern : string = InputPattern.EMAIL;
  userInfo : UserInfo = new UserInfo();
  destinationId : any;
  action : string;
  destination : Destination = new Destination();
  constructor(public sharedService : SharedService, 
    public utilityService : UtilityService,
    public apiService : ApiService, private _Activatedroute: ActivatedRoute) {
      this.districts = this.utilityService.getMasterByLookUpName('District');
      this.action  = this._Activatedroute.snapshot.data['path'];
      this.sharedService.castUser.subscribe(user => this.userInfo = user);
      this.destinationId = this._Activatedroute.snapshot.paramMap.get("id");
      

      this.destinationId = this._Activatedroute.snapshot.paramMap.get("id");
      if(this.destinationId) {
        this.apiService.getWithPathValue('Destinations', '/' + this.destinationId).subscribe((result: Response) => {
          if (result.isSuccess) {
            this.destination = result.data;
          }
        });
      }
    }
  ngOnInit(): void {
  }

  onClickOfAddImage() {
    if(!this.destination.images) {
      this.destination.images = [];
    }
    this.destination.images.push(new Images(false, ""));
  }

  onChangeImage(event : any, image : Images)  {
    this.apiService.upload("FileUpload", event.target.files[0]).subscribe((result: any) => {
      if(result.body.isSuccess) {
        image.path = result.body.data;
      }
    })
  }

  onClickOfSelectImage(hotelImage : Images) { 
    this.destination.images.forEach(img => img.isMainImage = false);

    hotelImage.isMainImage = true;
  }

  onClickOfDeleteImage(index : number) {
    this.destination.images.splice(index , 1);
  }

  onClickSubmit() {
    if(this.destinationId) {
    this.apiService.put('Destinations','',this.destination).subscribe((result : Response) => {
      if(result.isSuccess) {
       alert("Successfully updated");
      }
    })
  } else {
    this.destination.createdBy = this.userInfo.emailId;
    this.apiService.post('Destinations','',this.destination).subscribe((result : Response) => {
      if(result.isSuccess) {
        
        this.destinationId = result.data._id;
        alert("Successfully created");
      }
    })
  }
  }

}
