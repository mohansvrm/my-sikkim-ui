import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../core/services/api.service';
import {Response } from '../../core/models/Response';
import { UtilityService } from '../../core/services/utility.service';
import { Destination } from 'src/app/core/models/Destination';
import { Images } from 'src/app/core/models/Image';
@Component({
  selector: 'app-destination-list',
  templateUrl: './destination-list.component.html',
  styleUrls: ['./destination-list.component.css']
})
export class DestinationListComponent implements OnInit {
  confirmationModal: any = {};
  destinations : Destination [] = [];

  constructor(public apiService : ApiService, public utilityService : UtilityService) { }

  ngOnInit(): void {
    this.apiService.get('Destinations').subscribe((result : Response)  => {
      if(result.isSuccess) {
        this.destinations = result.data;
      } 
    });
  }

  callBackFromPopup(event : any) {
    // this.selectedHotel.status =  this.type;
    // if(!this.selectedHotel.comments) {
    //   this.selectedHotel.comments = [];
    // }
    // this.selectedHotel.comments.push({...event});
    // this.apiService.put('Hotels','/' +  this.selectedHotel._id,this.selectedHotel).subscribe(
    //   (result : Response) => {
    //   if(result.isSuccess) {
    //    alert("Successfully updated");
    //   }
    // })
  }

  getSrc(dest : Destination) { 
    if(dest.images) {
      const mainImage : Images | undefined = dest.images.find(image => image.isMainImage);
      if(mainImage) {
        return mainImage.path;
      }
    }
    return '';
   }

   onClickOfDelete(destination : any, index : number) {

   }

 


}
