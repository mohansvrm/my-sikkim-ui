import { Component, ElementRef, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../../core/services/api.service';
import {Response } from '../../core/models/Response';
import { UtilityService } from '../../core/services/utility.service';
import { SharedService } from 'src/app/core/services/shared.service';
import { Destination } from 'src/app/core/models/Destination';
declare var $: any;
@Component({
  selector: 'app-destination',
  templateUrl: './destination.component.html',
  styleUrls: ['./destination.component.css']
})
export class DestinationComponent implements OnInit {
  @ViewChild('product') product_slides!: ElementRef; 
  @ViewChildren('products')
  products!: QueryList<any>;
  destination : Destination = new Destination() ;
  id : string | null = '';
  comment : string = '';
  adultCount : number = 0;
  childrenCount : number = 0;
  selectDate : any;
  constructor(public apiService : ApiService, private _Activatedroute:ActivatedRoute ,
    public utilityService: UtilityService, public sharedService : SharedService) { 
    this.id=this._Activatedroute.snapshot.paramMap.get("id");
  }

  ngOnInit(): void {
    this.apiService.getWithPathValue('Destinations','/' + this.id).subscribe((result : Response) => {
      if(result.isSuccess) {
        this.destination = result.data; 
      }
    });

    // this.apiService.getWithPathValue('Cart','/smohan.thi@gmail.com').subscribe((result : Response)  => {
    //   if(result.isSuccess) {
    //     this.sharedService.headerInfo.cartCount = "";
    //     if(result.data.length) {
    //       this.sharedService.headerInfo.cartCount = result.data.length;

    //     }
    //   } 
    // });
  }

  ngAfterViewInit() {
    this.products.changes.subscribe(t => {
      this.rederoOwlCarousel();
    })
  }
 

  rederoOwlCarousel() {
    $(this.product_slides.nativeElement).owlCarousel({
      items: 1,
      margin: 0,
      loop: false,
      autoplay: true,
      autoplayTimeout: 5000,
      dots: false,
      nav: true,
      navText: [('<i class="lni lni-chevron-left"></i>'), ('<i class="lni lni-chevron-right"></i>')]
  })
  }

 
 
  }

