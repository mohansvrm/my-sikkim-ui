import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Alert } from 'src/app/core/models/Alert';
import { Hotel, HotelDocuments } from 'src/app/core/models/Hotel';
import { Images } from 'src/app/core/models/Image';
import { UserInfo } from 'src/app/core/models/UserInfo';
import { InputPattern } from 'src/app/core/pattern/InputPattern';
import { ApiService } from 'src/app/core/services/api.service';
import { SharedService } from 'src/app/core/services/shared.service';
import { UtilityService } from 'src/app/core/services/utility.service';
import { ConfirmDialogService } from 'src/app/shared/dialog/dialog.service';
import {Response } from '../../core/models/Response';

@Component({
  selector: 'app-hotel-form',
  templateUrl: './hotel-form.component.html',
  styleUrls: ['./hotel-form.component.css']
})
export class HotelFormComponent implements OnInit {
  hotelServices : any[] = [];
  hotelInfo : Hotel = new Hotel();
  hotelInfo_copy : Hotel = new Hotel();
  districts : any = [];
  emailPattern : string = InputPattern.EMAIL;
  userInfo : UserInfo = new UserInfo();
  hotelId : any;
  action : string;
  confirmationModal: any = {};
  @ViewChild('hotelForm') hotelForm!: any;
  constructor(public sharedService : SharedService, 
    public utilityService : UtilityService, public confirmDialogService : ConfirmDialogService,
    public apiService : ApiService, private _Activatedroute: ActivatedRoute) {
      this.hotelServices = this.utilityService.getMasterByLookUpName('HotelServices');
    
      this.districts = this.utilityService.getMasterByLookUpName('District');
      this.action  = this._Activatedroute.snapshot.data['path'];

      this.sharedService.castUser.subscribe(user => this.userInfo = user);

      
      this.hotelId = this._Activatedroute.snapshot.paramMap.get("id");
      if(this.hotelId) {
        this.apiService.getWithPathValue('Hotels', '/' + this.hotelId).subscribe((result: Response) => {
          if (result.isSuccess) {
            this.hotelInfo = result.data;
            this.hotelInfo_copy = JSON.parse(JSON.stringify(this.hotelInfo));
            if(!this.hotelInfo.amenities || !this.hotelInfo.amenities.length) {
            
            let ids = new Set(this.hotelInfo.amenities.map(d => d.code));
            this.hotelInfo.amenities = [...this.hotelInfo.amenities, ...this.hotelServices.filter(d => !ids.has(d.code))];
            }
          }
        });
      } else {
        this.hotelInfo.amenities = this.utilityService.getMasterByLookUpName('HotelServices');
      }
     }

 
  ngOnInit(): void {
   
  }

  ngAfterViewInit() {
    
    
  }

  onChangeImage(event : any, hotelImage : Images)  {
    this.apiService.upload("FileUpload", event.target.files[0]).subscribe((result: any) => {
      if(result.body.isSuccess) {
        hotelImage.path = result.body.data;
      }
    })
  }

  onClickOfDeleteImage(index : number) {
    this.hotelInfo.images.splice(index , 1);
  }
 

  onClickOfAddImage() {
    if(!this.hotelInfo.images) {
      this.hotelInfo.images = [];
    }
    this.hotelInfo.images.push(new Images(false,''))
  }

  onClickOfSelectImage(hotelImage : Images) { 
    this.hotelInfo.images.forEach(img => img.isMainImage = false);

    hotelImage.isMainImage = true;
  }

  onChangeFile(event : any, hotelDocs : HotelDocuments)  {
    this.apiService.upload("FileUpload", event.target.files[0]).subscribe((result: any) => {
      if(result.body.isSuccess) {
        hotelDocs.path = result.body.data;
      }
    })
  }

  onClickOfView(hotelImage : Images) {
    window.open(hotelImage.path, '_blank')?.focus();
  }

  onClickOfViewDoc(hotelDocs : HotelDocuments) {
    window.open(hotelDocs.path, '_blank')?.focus();
  }

  onClickOfDeleteFile(index : number) {
    this.hotelInfo.images.splice(index , 1);
  }
 

  onClickOfAddFile() {
    if(!this.hotelInfo.documents) {
      this.hotelInfo.images = [];
    }
    this.hotelInfo.documents.push(new HotelDocuments('',''))
  }
 

  type : string = '';
  onClickOfOpenComment(type: string) {
    this.type = type;
     
    let doc: any = document.getElementById("comments");
    doc.style.display = "block";
  }

  callBackFromPopup(event : any) {
    this.hotelInfo.status =  this.type;
    if(!this.hotelInfo.comments) {
      this.hotelInfo.comments = [];
    }
    this.hotelInfo.comments.push({...event});
    this.apiService.put('Hotels','/' +  this.hotelInfo._id,this.hotelInfo).subscribe(
      (result : Response) => {
      if(result.isSuccess) {
        this.sharedService.setAlert(new Alert('success','Successfully created',false));

      //  alert("Successfully updated");
      }
    })
  }


  onClickSubmit() {
      if(this.hotelId) {
        if(this.hotelInfo.status === 'Rejected') {
          this.hotelInfo.status = 'Initiated';
        }
        if(this.hotelInfo.status === 'Approved') {
          let isUpdated = false;
          if(JSON.stringify(this.hotelInfo.amenities) !== JSON.stringify(this.hotelInfo_copy.amenities)) {
            isUpdated = true;
          }
          if(JSON.stringify(this.hotelInfo.documents) !== JSON.stringify(this.hotelInfo_copy.documents)) {
            isUpdated = true;
          }
          if(JSON.stringify(this.hotelInfo.images) !== JSON.stringify(this.hotelInfo_copy.images)) {
            isUpdated = true;
          }
          if(isUpdated) {
            this.hotelInfo.status = 'Initiated';
            this.confirmDialogService.confirmThis("You have changed",() => {
              this.updateHotel()
            },() => {
              
            })
          }  
        } else {
          this.updateHotel();
        }
     
    } else {
      this.hotelInfo.hotelOwner = this.userInfo.emailId;
      this.apiService.post('Hotels','',this.hotelInfo).subscribe((result : Response) => {
        if(result.isSuccess) {
          
          this.hotelId = result.data._id;
          this.sharedService.setAlert(new Alert('success','Successfully created',false));

          // alert("Successfully created");
        }
      })
    }
  
  }

  updateHotel() {
    this.apiService.put('Hotels','/' + this.hotelId,this.hotelInfo).subscribe((result : Response) => {
      if(result.isSuccess) {
        this.sharedService.setAlert(new Alert('success','Successfully updated',false));
      //  alert("Successfully updated");
      }
    })
  }

}
