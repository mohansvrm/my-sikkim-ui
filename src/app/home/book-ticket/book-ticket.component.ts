import { Component,  OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../../core/services/api.service';
import {Response } from '../../core/models/Response';
import { UtilityService } from '../../core/services/utility.service';
import { Order } from '../../core/models/Cart';
import { SharedService } from 'src/app/core/services/shared.service';
import { WindowRefService } from 'src/app/core/services/window-ref.service';
import { UserInfo } from 'src/app/core/models/UserInfo';
import { jsPDF } from 'jspdf';
import html2canvas from 'html2canvas';
import { Destination } from 'src/app/core/models/Destination';
import { ConfigService } from 'src/app/core/services/config.service';


@Component({
  selector: 'app-book-ticket',
  templateUrl: './book-ticket.component.html',
  styleUrls: ['./book-ticket.component.css']
})
export class BookTicketComponent implements OnInit {
  destination : Destination = new Destination() ;
  id : string | null = '';
  comment : string = '';
  adultCount : number = 0;
  childrenCount : number = 0;
  selectDate : any;
  minDate : string = new Date().toISOString().split('T')[0];
  orders : Order[] = [];
  user : UserInfo = new UserInfo();
  constructor(private winRef: WindowRefService, public configService : ConfigService,
    public apiService : ApiService, private _Activatedroute:ActivatedRoute ,
    public utilityService: UtilityService, public sharedService : SharedService) {
      this.id=this._Activatedroute.snapshot.paramMap.get("id");
 }

  ngOnInit(): void {
    this.sharedService.castUser.subscribe(user => this.user = user);

    this.apiService.getWithPathValue('Destinations','/' + this.id).subscribe((result : Response) => {
      if(result.isSuccess) {
        this.destination = result.data; 
      }
    });
  }

  

  onClickOfAdultCount(action : string) {
    if(action === 'Plus') {
      this.adultCount++;
    } else {
      if(this.adultCount > 0) {
        this.adultCount--;
      }
    }
  }

  onClickOfChildrenCount(action : string) {
    if(action === 'Plus') {
      this.childrenCount++;
    } else {
      if(this.childrenCount > 0) {
        this.childrenCount--;
      }
    }
  }

  onClickOfBookTicket() {
    if(!this.user.idproof) {
      alert("Id proof is mandatory to book the tickets. Please upload the id proof in the profile page");
      return;
    }
    if(this.childrenCount < 1 && this.adultCount < 1) {
      alert("Adult and Children Count is empty");
      return;
    }
    let totalAmount = this.adultCount * this.destination.adultFee + this.childrenCount * this.destination.childrenFee;
    const options: any = {
      key: this.configService.getRazoreConfig().key,
      amount: totalAmount * 100,  // amount should be in paise format to display Rs 1255 without decimal point
      currency: this.configService.getRazoreConfig().currency,
      name: this.user.emailId, // 
      order_id: "" // order_id created by you in backend
    };
    options.handler = ((response: any, error:any) => {
      options.response = response;
      
      const cart : Order = new Order();
      cart.name = this.destination.destName;
      cart.adultCount = this.adultCount;
      cart.adultFee = this.destination.adultFee;
      cart.childrenCount = this.childrenCount;
      cart.childrenFee = this.destination.childrenFee;
      cart.orderDate = this.selectDate;
      cart.userName = this.user.emailId;
      cart.type = 'Order';
      cart.orderId = "";
      cart.razorPaymentId = response.razorpay_payment_id;
      cart.locationName = this.destination.address;
      this.apiService.post('Order','',cart).subscribe((result : Response) => {
        if(result.isSuccess) {
           this.orders = [cart];
           setTimeout(() =>{
            this.convetToPDF()
           },1000);
        }
      });

    });

    const rzp = new this.winRef.nativeWindow.Razorpay(options);
    rzp.open();

  }

  onClickOfAddToCart() {
    this.convetToPDF();
    const cart : Order = new Order();
    cart.name = this.destination.destName;
    cart.adultCount = this.adultCount;
    cart.adultFee = this.destination.adultFee;
    cart.childrenCount = this.childrenCount;
    cart.childrenFee = this.destination.childrenFee;
    cart.orderDate = this.selectDate;
    cart.userName = this.user.emailId;
    cart.type = 'Cart';
    cart.locationName = this.destination.address;
    this.apiService.post('Order','',cart).subscribe((result : Response) => {
      if(result.isSuccess) {
        //  this.convetToPDF();
      }
    });
  }

  public convetToPDF()
  {
    const data : any = document.getElementById('contentToConvert');
    html2canvas(data).then(canvas => {
      // Few necessary setting options
      var imgWidth = 300;
      var pageHeight = 200;
      var imgHeight = canvas.height * imgWidth / canvas.width;
      var heightLeft = imgHeight;

      const contentDataURL = canvas.toDataURL('image/png')
      let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF
      var position = 0;
      pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)
      pdf.save('new-file.pdf'); // Generated PDF
    });
  }
  

}
