import { Component, ElementRef, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../../core/services/api.service';
import {Response } from '../../core/models/Response';
import { UtilityService } from '../../core/services/utility.service';
import { Order } from '../../core/models/Cart';
import { SharedService } from 'src/app/core/services/shared.service';
import { WindowRefService } from 'src/app/core/services/window-ref.service';

@Component({
  selector: 'app-apply-permit',
  templateUrl: './apply-permit.component.html',
  styleUrls: ['./apply-permit.component.css']
})
export class ApplyPermitComponent implements OnInit {
  id : string | null = '';
  destination : any ;
  documentNumber : string = '';
  documentType : string = '';
  url : string = '';
  permitType : string = 'Individual';
  listOfPermits : any = [];
  constructor(private winRef: WindowRefService,public apiService : ApiService, private _Activatedroute:ActivatedRoute ,
    public utilityService: UtilityService, public sharedService : SharedService) { 
    this.id=this._Activatedroute.snapshot.paramMap.get("id");
  }

  ngOnInit(): void {
    this.apiService.getWithPathValue('Destinations','/' + this.id).subscribe((result : Response) => {
      if(result.isSuccess) {
        this.destination = result.data; 
        // this.onClickOfLevel(this.istype);
      }
    });
  }

  onClickOfAdd() {
    
    this.listOfPermits.push({
      locationName : this.destination.destName,
      documentNumber : '',
      documentType : '',
      photo : {},
      userName : "smohan.thi@gmail.com",
      permitType : this.permitType 
    });
    
  }
  onSelectPhoto(event : any, permit : any) { 
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]);  

      reader.onload = (event:any) => {
        this.url =event.target.result; 
      // 
      }
    }
	}

  onClickOfDelete(index : number) {
    this.listOfPermits.splice(index , 1);
  }

  onClickSubmit() {
    if(this.permitType === 'Individual') {
      // let permit = {
      //   locationName : this.destination.destName,
      //   documentNumber : this.documentNumber,
      //   documentType : this.documentType,
      //   photo : {},
      //   userName : "smohan.thi@gmail.com"
      // }
      this.listOfPermits = [{
        permitType : this.permitType, 
        locationName : this.destination.destName,
        documentNumber : this.documentNumber,
        documentType : this.documentType,
        photo : {},
        userName : "smohan.thi@gmail.com"
      }];
    }
    
   

    // const base : any=  this.url.split(";");
    // if(base.length) {
    //   permit.photo = {
    //     image : base[1].split(",")[1],
    //     contentType : base[0].split(":")[1]
    //   }
    // }

    this.apiService.post("Permit",'', this.listOfPermits).subscribe((result: Response) => {
      if(result.isSuccess) {
        const options: any = {
          key: 'rzp_test_wS3m6RchQD9wqP',
          amount: this.destination.permitFee, // amount should be in paise format to display Rs 1255 without decimal point
          currency: 'INR',
          name: 'smohan.thi@gmail.com', // 
          order_id: "" // order_id created by you in backend
        };
        options.handler = ((response: any, error:any) => {
          options.response = response;
          alert("Payment Success")
          
          
          // call your backend api to verify payment signature & capture transaction
        });
      
        const rzp = new this.winRef.nativeWindow.Razorpay(options);
        rzp.open();
        
      }
    });
    
  }

}
