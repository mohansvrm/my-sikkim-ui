import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserInfo } from 'src/app/core/models/UserInfo';
import { SharedService } from '../../core/services/shared.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  userInfo : UserInfo = new UserInfo();
  constructor(public sharedService : SharedService,private router: Router ) { 
    this.sharedService.castUser.subscribe(user => this.userInfo = user);
  }

  ngOnInit(): void {
  }

  signOut() {
    // this.socialAuthServive.signOut().then(() => this.router.navigate(['login']));
  }

  onClickApplyFilter() {
    this.sharedService.setFilter(true);
  }

}
