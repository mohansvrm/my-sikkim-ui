import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { SharedService } from '../../core/services/shared.service';

@Component({
  selector: 'app-comments-popup',
  templateUrl: './comments-popup.component.html',
  styleUrls: ['./comments-popup.component.css']
})
export class CommentsPopupComponent implements OnInit {

  constructor(public sharedService : SharedService) { }
  @Input() confirmationModal : any;
  @Output() actionClick : EventEmitter<any> = new EventEmitter();
  comment : string = '';
  ngOnInit(): void {
  }

  onClickSubmit() {
    if(!this.comment) {
      return;
    }
    this.actionClick.emit({comment : this.comment,time : new Date()});
    this.onClickOfActionInPopup();

  }

  onClickOfActionInPopup() {
    let doc : any =  document.getElementById("comments");
     doc.style.display = "none";
  }
  
}
