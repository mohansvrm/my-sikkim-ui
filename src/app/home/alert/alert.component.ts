import { Component, OnInit } from '@angular/core';
import { Alert } from 'src/app/core/models/Alert';
import { SharedService } from 'src/app/core/services/shared.service';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.css']
})
export class AlertComponent implements OnInit {
  alert : Alert = new Alert();
  constructor(private sharedService : SharedService) { 
    this.sharedService.triggerAlert.subscribe(res => {
      this.alert = res
      setTimeout(() => {
        this.alert.isClosed = true;
      },3000);
    });
  }

  ngOnInit(): void {
  }

  onClickOfClose() {
    this.alert.isClosed = true;
  }

}
