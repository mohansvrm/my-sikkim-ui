import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { MainComponent } from './main/main.component';
import { HotelsComponent } from './hotels/hotels.component';
import { HotelComponent } from './hotel/hotel.component';
import { DestinationsComponent } from './destinations/destinations.component';
import { DestinationComponent } from './destination/destination.component';
import { HomeResolverService } from './home-resolver.service';
import { TreksComponent } from './treks/treks.component';
import { ExperienceComponent } from './experience/experience.component';
import { TravelAgentComponent } from './travel-agent/travel-agent.component';
import { EditProfileComponent } from './editProfile/editProfile.component';
import { ProfileComponent } from './profile/profile.component';
import { AuthGuardService } from '../core/services/auth-guard.service';
import { TrekComponent } from './trek/trek.component';
// import { AdventuresComponent } from './adventures/adventures.component';
import { CuisinesComponent } from './cuisines/cuisines.component';
import { ExperienceDetailsComponent } from './experience-details/experience-details.component';
import { TicketsComponent } from './tickets/tickets.component';
import { FeedbackComponent } from './feedback/feedback.component';
import { FAQComponent } from './FAQ/FAQ.component';
// import { ExperienceComponent } from './experience/experience.component';

import { FormsModule } from '@angular/forms';
import { ExperienceHomeComponent } from './experience-home/experience-home.component';
import { HotelFormComponent } from './hotel-form/hotel-form.component';
import { CartComponent } from './cart/cart.component';
import { PermitInfoComponent } from './permit-info/permit-info.component';
import { ApplyPermitComponent } from './apply-permit/apply-permit.component';
import { BookTicketComponent } from './book-ticket/book-ticket.component';
import { HotelListComponent } from './hotel-list/hotel-list.component';
import {  AddAgentComponent } from './add-agent/add-agent.component';
import { MyOrdersComponent } from './my-orders/my-orders.component';
import { CommentsPopupComponent } from './comments-popup/comments-popup.component';
import { DestinationListComponent } from './destination-list/destination-list.component';
import { TravelagentListComponent } from './agent-list/agent-list.component';
import { DestinationFormComponent } from './destination-form/destination-form.component';
import { AdminListComponent } from './admin-list/admin-list.component';
import { AdminFormComponent } from './admin-form/admin-form.component';

const routes: Routes = [

  {
    path: '', component: MainComponent,
    children: [
      {
        path: '', component: HomeComponent,
        resolve: {
          resolver: HomeResolverService
        },
        data: { path: 'home' }
      },
      {
        path: 'hotels', component: HotelsComponent,
        resolve: {
          resolver: HomeResolverService
        },
        data: { path: 'hotels' }
      },
    
      
      {
        path: 'edit-profile', component: EditProfileComponent,
        resolve: {
          resolver: HomeResolverService
        },
        data: { path: 'editprofile' }
      },
      {
        path: 'profile', component: ProfileComponent,
        resolve: {
          resolver: HomeResolverService
        },
        data: { path: 'profile' }
      },
      {
        path: 'hotel/:id', component: HotelComponent,
        resolve: {
          resolver: HomeResolverService
        },
        data: { path: 'hotel' }
      },
      {
        path: 'destinations', component: DestinationsComponent,
        resolve: {
          resolver: HomeResolverService
        },
        data: { path: 'destinations' }
      },
      {
        path: 'destination/:id', component: DestinationComponent,
        resolve: {
          resolver: HomeResolverService
        },
        data: { path: 'destination' }
      },
      {
        path: 'hotel/:id', component: HotelComponent,
        resolve: {
          resolver: HomeResolverService
        },
        data: { path: 'hotel' }
      },
      {
        path: 'treks', component: TreksComponent,
        resolve: {
          resolver: HomeResolverService
        },
        data: { path: 'treks' }
      },
      {
        path: 'experience-home', component: ExperienceHomeComponent,
        resolve: {
          resolver: HomeResolverService
        },
        data: { path: 'experience-home' }
      },
      {
        path: 'FAQ', component: FAQComponent,
        resolve: {
          resolver: HomeResolverService
        },
        data: { path: 'FAQ' }
      },
      {
        path: 'experiences/:type', component: ExperienceComponent,
        resolve: {
          resolver: HomeResolverService
        },
        data: { path: 'experiences' }
      },
      {
        path: 'experience/:type/:id', component: ExperienceDetailsComponent,
        resolve: {
          resolver: HomeResolverService
        },
        data: { path: 'experience' }
      },
      {
        path: 'travel-agents', component: TravelAgentComponent,
        resolve: {
          resolver: HomeResolverService
        },
        data: { path: 'travel-agents' }
      },
      {
        path: 'trek/:id', component: TrekComponent,
        resolve: {
          resolver: HomeResolverService
        },
        data: { path: 'trek' }
      },
      {
        path: 'tickets', component: TicketsComponent,
        resolve: {
          resolver: HomeResolverService
        },
        data: { path: 'tickets' }
      },
      {
        path: 'book-ticket/:id', component: BookTicketComponent,
        resolve: {
          resolver: HomeResolverService
        },
        data: { path: 'book-ticket' }
      },
      {
        path: 'permit-info', component: PermitInfoComponent,
        resolve: {
          resolver: HomeResolverService
        },
        data: { path: 'permit-info' }
      },
      {
        path: 'apply-permit/:id', component: ApplyPermitComponent,
        resolve: {
          resolver: HomeResolverService
        },
        data: { path: 'apply-permit' }
      },
      {
        path: 'feedback', component: FeedbackComponent,
        resolve: {
          resolver: HomeResolverService
        },
        data: { path: 'feedback' }
      },
      {
        path: 'add-agent', component: AddAgentComponent,
        resolve: {
          resolver: HomeResolverService
        },
        data: { path: 'add-agent' }
      },

      {
        path: 'add-hotel', component: HotelFormComponent,
        resolve: {
          resolver: HomeResolverService
        },
        data: { path: 'add-hotel' }
      },
      {
        path: 'edit-hotel/:id', component: HotelFormComponent,
        resolve: {
          resolver: HomeResolverService
        },
        data: { path: 'edit-hotel' }
      },
      {
        path: 'edit-agent/:id', component: AddAgentComponent,
        resolve: {
          resolver: HomeResolverService
        },
        data: { path: 'edit-agent' }
      },
      {
        path: 'view-hotel/:id', component: HotelFormComponent,
        resolve: {
          resolver: HomeResolverService
        },
        data: { path: 'view-hotel' }
      },
      {
        path: 'view-agent/:id', component: AddAgentComponent,
        resolve: {
          resolver: HomeResolverService
        },
        data: { path: 'view-agent' }
      },
      {
        path: 'cart-details', component: CartComponent,
        resolve: {
          resolver: HomeResolverService
        },
        data: { path: 'cart' }
      },
      {
        path: 'my-order', component: MyOrdersComponent,
        resolve: {
          resolver: HomeResolverService
        },
        data: { path: 'my-order' }
      },
      {
        path: 'hotel-list', component: HotelListComponent,
        resolve: {
          resolver: HomeResolverService
        },
        data: { path: 'hotel-list' }
      }  ,
      {
        path: 'destination-list', component: DestinationListComponent,
        resolve: {
          resolver: HomeResolverService
        },
        data: { path: 'destination-list' }
      },
      {
        path: 'travelagent-list', component: TravelagentListComponent,
        resolve: {
          resolver: HomeResolverService
        },
        data: { path: 'travelagent-list' }
      },
      {
        path: 'add-destination', component: DestinationFormComponent,
        resolve: {
          resolver: HomeResolverService
        },
        data: { path: 'add-destination' }
      },
      {
        path: 'edit-destination/:id', component: DestinationFormComponent,
        resolve: {
          resolver: HomeResolverService
        },
        data: { path: 'edit-destination' }
      },
      {
        path: 'admin-list', component: AdminListComponent,
        resolve: {
          resolver: HomeResolverService
        },
        data: { path: 'admin-list' }
      }
      ,
      {
        path: 'add-admin', component: AdminFormComponent,
        resolve: {
          resolver: HomeResolverService
        },
        data: { path: 'add-admin' }
      },
      {
        path: 'view-admin/:id', component: AdminFormComponent,
        resolve: {
          resolver: HomeResolverService
        },
        data: { path: 'view-admin' }
      }
    ],
    // canActivate : [AuthGuardService]
  },


];

@NgModule({
  imports: [RouterModule.forChild(routes),
    FormsModule],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
