import { Component, ElementRef, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../../core/services/api.service';
import { Response } from '../../core/models/Response';
import { DomSanitizer } from '@angular/platform-browser';
import { UtilityService } from 'src/app/core/services/utility.service';
declare var $: any;
@Component({
  selector: 'app-hotel',
  templateUrl: './hotel.component.html',
  styleUrls: ['./hotel.component.css']
})
export class HotelComponent implements OnInit {
  @ViewChild('product') product_slides!: ElementRef;
  @ViewChildren('products')
  products!: QueryList<any>;
  hotel: any;
  id: string | null = '';
  comment: string = '';
  rating: number = 0;

  constructor(public apiService: ApiService, private _Activatedroute: ActivatedRoute,
    public domSanitizer: DomSanitizer, public utilityService: UtilityService) {
    this.id = this._Activatedroute.snapshot.paramMap.get("id");
  }

  ngOnInit(): void {
    this.apiService.getWithPathValue('Hotels', '/' + this.id).subscribe((result: Response) => {
      if (result.isSuccess) {
        this.hotel = result.data;
        // for (const image of this.hotel.images) {
        //   // 
        //   image.base = this.utilityService.convertArrayToBase64(image.data.data, image.contentType);
        // }
      }
    });
  }

  
  ngAfterViewInit() {
    this.products.changes.subscribe(t => {
      this.rederoOwlCarousel();
    })
  }
 

  rederoOwlCarousel() {
    $(this.product_slides.nativeElement).owlCarousel({
      items: 1,
      margin: 0,
      loop: false,
      autoplay: true,
      autoplayTimeout: 5000,
      dots: false,
      nav: true,
      navText: [('<i class="lni lni-chevron-left"></i>'), ('<i class="lni lni-chevron-right"></i>')]
    })
  }
  onClickRating(rating: number) {
    this.rating = rating;
  }

  getCount(rating: number) {
    const tmpArray: number[] = [];
    tmpArray[rating - 1] = rating;
    return tmpArray;
  }

  onClickSubmit(): void {
    if (!this.hotel.reviews) {
      this.hotel.reviews = [];
    }

    this.hotel.reviews.push({ reviewComment: this.comment, rating: this.rating })
    this.apiService.put('Hotels', '/' + this.id, this.hotel).subscribe((result: Response) => {
      if (result.isSuccess) {
        this.hotel = result.data;
      }
    });
  }
 

}
