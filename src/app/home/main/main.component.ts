import { Component, OnInit } from '@angular/core';
import { UserInfo } from 'src/app/core/models/UserInfo';
import { SharedService } from 'src/app/core/services/shared.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  userInfo: UserInfo = new UserInfo();
  constructor(public sharedService: SharedService) { 
    this.sharedService.castUser.subscribe(user => this.userInfo = user);
  }

  ngOnInit(): void {
  }

}
