import { Injectable } from '@angular/core';
import { Router, Resolve, RouterStateSnapshot, ActivatedRouteSnapshot, ActivatedRoute } from '@angular/router';
import { ApiService } from '../core/services/api.service';
import { SharedService } from '../core/services/shared.service';
import { UtilityService } from '../core/services/utility.service';

@Injectable({
  providedIn: 'root'
})
export class HomeResolverService implements Resolve<boolean> {

  constructor( private router: Router, public sharedService : SharedService,  
    public apiService : ApiService,public utilityService : UtilityService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    // 
   
    const pathName : string = route.data['path'] ? route.data['path'] : '';
     if (pathName === 'home') {
      this.setHeaderInfo(true,true,false,'',false,'',false);
    } else if (pathName === 'hotels') {
      const filters : any = [
        {
          name : "District",
          list : this.utilityService.getMasterByLookUpName('District')
        }
      ];
      this.setHeaderInfo(false,false,true,'/home',true,'Hotels',true,false,filters);
    }  else if (pathName === 'hotel') {
      this.setHeaderInfo(false,true,true,'/home/hotels',true,'Hotels',false);
    }
    else if (pathName === 'destinations') {
      const filters : any = [
        {
          name : "District",
          list : this.utilityService.getMasterByLookUpName('District')
        }
      ];
      this.setHeaderInfo(false,false,true,'/home',true,'Destinations',true, false,filters);
    }
    else if (pathName === 'destination') {
      this.setHeaderInfo(false,true,true,'/home/destinations',true,'Destination Detail',false, false);
    }
    else if (pathName === 'tickets') {
      this.setHeaderInfo(false,true,true,'/home',true,'Book Ticket',false);
    }
    else if (pathName === 'book-ticket') {
      this.setHeaderInfo(false,false,true,'/home/tickets',true,'Book Ticket',false,true);
    }
    else if (pathName === 'FAQ') {
      this.setHeaderInfo(false,true,true,'/home',true,'FAQ',false, false);
    }
    else if (pathName === 'treks') {
      this.setHeaderInfo(false,true,true,'/home',true,'Trekking',false);
    }
    else if (pathName === 'experience-home') {
      this.setHeaderInfo(true,true,false,'',true,'Experiences',false);
    }
    else if (pathName === 'experiences') { 
      const type = route.paramMap.get('type')!;
      this.setHeaderInfo(false,true,true,'/home/experience-home',true,type,false);
    }
    else if (pathName === 'experience') { 
      const type = route.paramMap.get('type')!;
      this.setHeaderInfo(false,true,true,'/home/experience-home',true,type + ' Information',false);
    }
    else if (pathName === 'editprofile') { 
      this.setHeaderInfo(true,true,false,'',true, '',false);
    }
    else if (pathName === 'profile') { 
      this.setHeaderInfo(true,true,false,'',true, '',false);
    }
    else if (pathName === 'add-agent') {
      this.setHeaderInfo(false,true,true,'/home',true,'Travel Agents',false);
    }
    else if (pathName === 'trek') {
      this.setHeaderInfo(false,true,true,'/home/treks',true,'Trek Detail',false);
    }
    else if (pathName === 'feedback') {
      this.setHeaderInfo(false,true,true,'/home',true,'Feedback',false);
    }else if (pathName === 'PostTravelAgent') {
      this.setHeaderInfo(false,true,true,'/home',true,'PostTravelAgent',false);
    }
   
    return true;
    
  }

  setHeaderInfo(logo : boolean, navBtn: boolean, backBtn : boolean, backUrl : string,
    heading : boolean, headingText : string, filterBtn : boolean, cartIcon :boolean = false, filterList : any = []) {
    this.sharedService.headerInfo.logo = logo;
    this.sharedService.headerInfo.navBtn = navBtn;
    this.sharedService.headerInfo.backBtn = backBtn;
    this.sharedService.headerInfo.backUrl = backUrl;
    this.sharedService.headerInfo.heading = heading;
    this.sharedService.headerInfo.headingText = headingText;
    this.sharedService.headerInfo.filterBtn = filterBtn;
    this.sharedService.headerInfo.cartIcon = cartIcon;
    this.sharedService.headerInfo.filters = filterList;
  }
}
