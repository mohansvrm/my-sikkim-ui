import { Component,ElementRef, OnInit,ViewChild} from '@angular/core';
import { ApiService } from '../../core/services/api.service';
import {Response } from '../../core/models/Response';
import { Router } from '@angular/router';
import { SharedService } from 'src/app/core/services/shared.service';
import { UtilityService } from 'src/app/core/services/utility.service';
import { UserInfo } from 'src/app/core/models/UserInfo';
 
declare var $: any;

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
user : UserInfo = new UserInfo();
constructor(private router: Router, 
  public sharedService : SharedService,
  public apiService: ApiService,  public utilityService: UtilityService) { }

  ngOnInit(): void {
    this.sharedService.castUser.subscribe(user => this.user = user);
    // this.user = this.sharedService.userInfo;
    // this.apiService.getWithPathValue("User", "/"+this.sharedService.userInfo.emailId).subscribe((result: Response) => {
    //   if(result.isSuccess) {
    //     
    //     this.user = result.data;
    //   }
    // });
  }}
