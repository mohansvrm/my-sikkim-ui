declare var $: any;
import { Component, ElementRef, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../../core/services/api.service';
import {Response } from '../../core/models/Response';
import { UtilityService } from '../../core/services/utility.service';
import { Experience, ExperienceImages } from 'src/app/core/models/experience';
import { Images } from 'src/app/core/models/Image';
@Component({
  selector: 'app-experience',
  templateUrl: './experience.component.html',
  styleUrls: ['./experience.component.css']
})
export class ExperienceComponent implements OnInit {
  @ViewChild('carousel') hero_slides!: ElementRef;
  @ViewChild('collection') collection_slides!: ElementRef;
  
    listOfExperience : any = [];
    type : string | null= '';
    constructor(public apiService : ApiService, private _Activatedroute:ActivatedRoute ,
      public utilityService: UtilityService) { 
      this.type=this._Activatedroute.snapshot.paramMap.get("type");
       
    }
  
    ngOnInit(): void {

      this.apiService.getWithPathValue('ExperienceType', this.type).subscribe((result : Response) => {
        if(result.isSuccess) {
          
          this.listOfExperience = result.data; 
          // for (const image of this.destination.images) {
          //   // 
          //   image.base = this.utilityService.convertArrayToBase64(image.data.data, image.contentType);
          // }
        }
      });
    }

    // getSrc(experience : any) { 
    //   return this.utilityService.convertArrayToBase64SafeUrl(experience.image.data,experience.contentType);
    //  }
    getSrc(experience : Experience) { 
      if(experience.images) {
        const mainImage : Images | undefined = experience.images.find(image => image.isMainImage);
        if(mainImage) {
          return mainImage.path;
        }
      }
      return '';
     }
     
  


    ngAfterViewInit() {
  
  
      $(this.hero_slides.nativeElement).owlCarousel({
        items: 1,
        loop: true,
        autoplay: true,
        dots: true,
        center: true,
        margin: 0,
        animateIn: 'fadeIn',
        animateOut: 'fadeOut'
      });
  
      $(this.collection_slides.nativeElement).owlCarousel({
          items: 2,
          margin: 16,
          loop: true,
          autoplay: true,
          smartSpeed: 800,
          dots: false,
          nav: false,
          responsive: {
            1400: {
              items: 3,
            },
            992: {
              items: 3,
            },
            768: {
              items: 3,
            },
            360: {
              items: 1,
            },
          },
        });
    
    }
}
