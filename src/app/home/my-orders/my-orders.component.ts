import { Component, OnInit } from '@angular/core';
import { Order } from 'src/app/core/models/Cart';
import { UserInfo } from 'src/app/core/models/UserInfo';
import { ApiService } from 'src/app/core/services/api.service';
import { SharedService } from 'src/app/core/services/shared.service';
import { UtilityService } from 'src/app/core/services/utility.service';
import { WindowRefService } from 'src/app/core/services/window-ref.service';
import {Response } from '../../core/models/Response';
import { jsPDF } from 'jspdf';
import html2canvas from 'html2canvas';

@Component({
  selector: 'app-my-orders',
  templateUrl: './my-orders.component.html',
  styleUrls: ['./my-orders.component.css']
})
export class MyOrdersComponent implements OnInit {

  listOfOrders : Order[] = [];
  totalAmount : number = 0;
  user : UserInfo = new UserInfo();
  constructor(private winRef: WindowRefService,public apiService : ApiService,  
    public utilityService: UtilityService, public sharedService : SharedService) {
      this.sharedService.castUser.subscribe(user => this.user = user);
    }

  ngOnInit(): void {
    this.apiService.getWithPathValue('Order','/Order/'+this.user.emailId).subscribe((result : Response)  => {
      if(result.isSuccess) {
        if(result.data.length) {
          this.listOfOrders = result.data;
           
        }
      } 
    });
  }

  public convetToPDF()
  {
    const data : any = document.getElementById('contentToConvert');
    html2canvas(data).then(canvas => {
      // Few necessary setting options
      var imgWidth = 300;
      var pageHeight = 200;
      var imgHeight = canvas.height * imgWidth / canvas.width;
      var heightLeft = imgHeight;

      const contentDataURL = canvas.toDataURL('image/png')
      let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF
      var position = 0;
      pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)
      pdf.save('new-file.pdf'); // Generated PDF
    });
  }

}
