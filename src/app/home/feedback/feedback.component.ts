import { Component, OnInit } from '@angular/core';
import { UserInfo } from 'src/app/core/models/UserInfo';
import { ApiService } from 'src/app/core/services/api.service';
import { SharedService } from 'src/app/core/services/shared.service';
import { UtilityService } from 'src/app/core/services/utility.service';
import { Response } from '../../core/models/Response';

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.css']
})
export class FeedbackComponent implements OnInit {
  comment: string = '';
  rating: number = 0;
  image: any;
  userInfo : UserInfo = new UserInfo();
  constructor(public apiService: ApiService,   public utilityService: UtilityService,
    public sharedService: SharedService) {
      this.sharedService.castUser.subscribe(user => this.userInfo = user);
     }

  ngOnInit(): void {
  }

  onClickRating(rating: number) {
    this.rating = rating;
  }
  onClickSubmit(): void {
  
    this.apiService.post('FeedBack','', {name: this.userInfo.userName,
    mailId: this.userInfo.emailId,
    reviewComment: this.comment,
    rating:this.rating}).subscribe((result: Response) => {
      if (result.isSuccess) {
        
      }
    });
  }

  onSelectFile(event : any, prop : string) {
    this.apiService.upload("FileUpload", event.target.files[0]).subscribe((result: any) => {
      if(result.body.isSuccess) {
        if(prop === 'photo') {
          this.image = result.body.data;
          
        //   // this.sharedService.setUser( this.user);
        } else if(prop === 'bill') {
          this.image = result.body.data;
        }
        
      }
    })
  }

}
