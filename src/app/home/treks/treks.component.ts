import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../core/services/api.service';
import {Response } from '../../core/models/Response';
import { DomSanitizer } from '@angular/platform-browser';
import { UtilityService } from 'src/app/core/services/utility.service';
import { Trek, TrekImages } from 'src/app/core/models/Trek';
import { Images } from 'src/app/core/models/Image';

@Component({
  selector: 'app-treks',
  templateUrl: './treks.component.html',
  styleUrls: ['./treks.component.css']
})
export class TreksComponent implements OnInit {
  treks : any = [];
  level : string = 'Easy';

  constructor(public apiService : ApiService ,public domSanitizer  : DomSanitizer, public utilityService : UtilityService ) { }

  ngOnInit(): void {
    this.onClickOfLevel(this.level);
  }

  onClickOfLevel(level : string) : void {
    this.level = level;
    this.apiService.getWithPathValue('TreksLevel','/'+ this.level).subscribe((result :Response ) => {
      if(result.isSuccess) {
        this.treks = result.data;
    }})
  }
  // getSrc(trek : any) { 
  //   return this.utilityService.convertArrayToBase64SafeUrl(trek.image.data,trek.contentType);
  //  }

  getSrc(trek : Trek) { 
    if(trek.images) {
      const mainImage : Images | undefined = trek.images.find(image => image.isMainImage);
      if(mainImage) {
        return mainImage.path;
      }
    }
    return '';
   }

}
