import { Component,ElementRef, OnInit,ViewChild} from '@angular/core';
import { ApiService } from '../../core/services/api.service';
import {Response } from '../../core/models/Response';
declare var $: any;

@Component({
  selector: 'app-adventures',
  templateUrl: './cuisines.component.html',
  styleUrls: ['./cuisines.component.css']
})
export class CuisinesComponent implements OnInit {
@ViewChild('carousel') hero_slides!: ElementRef;
@ViewChild('collection') collection_slides!: ElementRef;

  destinations : any = [];

  constructor() { }

  ngOnInit(): void {
 
  }

  ngAfterViewInit() {


    $(this.hero_slides.nativeElement).owlCarousel({
      items: 1,
      loop: true,
      autoplay: true,
      dots: true,
      center: true,
      margin: 0,
      animateIn: 'fadeIn',
      animateOut: 'fadeOut'
    });

    $(this.collection_slides.nativeElement).owlCarousel({
        items: 2,
        margin: 16,
        loop: true,
        autoplay: true,
        smartSpeed: 800,
        dots: false,
        nav: false,
        responsive: {
          1400: {
            items: 3,
          },
          992: {
            items: 3,
          },
          768: {
            items: 3,
          },
          360: {
            items: 1,
          },
        },
      });
  
  }

}