import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GoogleLoginProvider, SocialAuthService} from 'angularx-social-login';
import { UserInfo } from '../core/models/UserInfo';
import { ApiService } from '../core/services/api.service';
import { SharedService } from '../core/services/shared.service';
import { UtilityService } from '../core/services/utility.service';
import { Response } from '../core/models/Response';
import { InputPattern } from '../core/pattern/InputPattern';
import { ConfirmDialogService } from '../shared/dialog/dialog.service';
@Component({
  selector: 'app-user-register',
  templateUrl: './user-register.component.html',
  styleUrls: ['./user-register.component.css']
})
export class UserRegisterComponent implements OnInit {

  userInfo : UserInfo = new UserInfo();
  confirmPassword : string = '';
  type : any;
  emailPattern : string = InputPattern.EMAIL;// "^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+$";
  phoneNoPatter : string = InputPattern.PHONE_NUMBER; //"^((\\+91-?)|0)?[0-9]{10}$";
  pwdPattern : string = InputPattern.PASSWORD; //"^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$";
  constructor(private router: Router,private socialAuthService: SocialAuthService, 
    public sharedService : SharedService,private confirmDialogService: ConfirmDialogService,
    public apiService: ApiService,  public utilityService: UtilityService,
    private _Activatedroute:ActivatedRoute) { 
      this.userInfo.userType =this._Activatedroute.snapshot.paramMap.get("type");
    }

  ngOnInit(): void {
}
 

  onClickOfSubmit() {
    if (this.userInfo.password !== this.confirmPassword) {
      alert("Password is mismatch");
      return;
    }
    if (this.userInfo.userName && this.userInfo.phoneNumber &&
      this.userInfo.emailId && this.userInfo.password) {
      this.apiService.post("User", "/Register", this.userInfo).subscribe((result: Response) => {
        if (result.isSuccess) {
          this.confirmDialogService.confirmThis("Successfully created, Do you want procced with login?", () => {
            this.router.navigate(['/login', this.userInfo.userType])
          },
            () => {

            })
        } else {
          alert(result.data);
        }
      });
    } else {
      alert("Fill all mandatory details");
    }

  }

}
