import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {GoogleLoginProvider, SocialAuthService} from 'angularx-social-login';
import { UserInfo } from '../core/models/UserInfo';
import { ApiService } from '../core/services/api.service';
import { SharedService } from '../core/services/shared.service';
import { UtilityService } from '../core/services/utility.service';
import { Response } from '../core/models/Response'; 
import { InputPattern } from '../core/pattern/InputPattern';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  emailPattern : string = InputPattern.EMAIL;// "^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+$";
  pwdPattern : string = InputPattern.PASSWORD; //"^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$";
  type : any;
  loginDetails : any = {emailId : '', password : '', userType : '', gAuth : false}
  constructor(private router: Router,private socialAuthService: SocialAuthService, 
    public sharedService : SharedService,
    public apiService: ApiService,  public utilityService: UtilityService,
    private _Activatedroute:ActivatedRoute) {
      
     }

  ngOnInit(): void {
    this.loginDetails.userType=this._Activatedroute.snapshot.paramMap.get("type");
  }

 
  onClickOfLogin() {
    let queryString = '?';
    for(const prop in this.loginDetails) {
      queryString = queryString + prop + '=' + this.loginDetails[prop] + '&';
    }
    this.apiService.getWithQueryString("User",queryString).subscribe((result: Response) => {
      
      if(result.isSuccess) {
        this.sharedService.setUser(result.data);
        if(result.data.userType === 'ADMIN' && result.data.isTempPwd ) {
          this.router.navigate(['/reset-password','temp']);
        } else {
          this.router.navigate(['/home']);
        }
        
      } else {
        alert(result.data)
      }
    },error => {
      
    });
    
  }

  loginWithGoogle(): void {
    // this.router.navigate(['/home']);
    // this.sharedService.userInfo.emailId = "subhajit.biswas@gmail.com";
    //     this.sharedService.userInfo.userName = "Subhajit Biswas";
    //     this.sharedService.userInfo.fullName = "Subhajit Biswas";;
    //     this.sharedService.userInfo.profile = ""; 
    this.socialAuthService.signIn(GoogleLoginProvider.PROVIDER_ID)
      .then((res) => {
        let userInfo : UserInfo = new UserInfo();
        userInfo.emailId = res.email;
        userInfo.userName = res.email;
        userInfo.fullName = res.name;
        userInfo.profile = res.photoUrl; 
        userInfo.gAuth = true;
        userInfo.userType = this.loginDetails.userType;
        this.apiService.post("User",'/Login', userInfo).subscribe((result: Response) => {
          if(result.isSuccess) {
            this.sharedService.setUser(result.data);
            this.router.navigate(['/home']);
          }
        });
  

      
      });
  }



}
