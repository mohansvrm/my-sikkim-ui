import { Injectable } from '@angular/core';
import { HttpClient , HttpHeaders, HttpParams, HttpRequest} from '@angular/common/http'; 
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  
  httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json'}) }; 
  url: string  = environment.APIEndpoint;
  constructor(private http : HttpClient) { }

  get(controllerKey: string): Observable<any> {
    let getUrl = this.url + this.getContollerName(controllerKey);
    return this.http.get(getUrl,this.httpOptions);
  }


  getWithPathValue(controllerKey: string,pathValue: any): Observable<any> {
    let getUrl = this.url + this.getContollerName(controllerKey) + pathValue;
    return this.http.get(getUrl,this.httpOptions);
  }

  getWithQueryString(controllerKey: string,queryString: string): Observable<any> {
    let getUrl = this.url + this.getContollerName(controllerKey) + queryString;
    return this.http.get(getUrl,this.httpOptions);
  }

  post(controllerKey: string,pathVariable: any, request: any) : Observable<any> {
    let postUrl = this.url + this.getContollerName(controllerKey) + pathVariable;
    return this.http.post(postUrl,request,this.httpOptions)
  }

  put(controllerKey: string, pathVariable: any, request: any) : Observable<any> {
    let postUrl = this.url + this.getContollerName(controllerKey) + pathVariable;
    return this.http.put(postUrl,request,this.httpOptions);
  }


  delete(controllerKey: string, pathVariable: any) : Observable<any> {
    let deleteUrl = this.url + this.getContollerName(controllerKey) + pathVariable;
    return this.http.delete(deleteUrl,this.httpOptions);
  }

  upload(controllerKey: string, file: File): Observable<any> {
    const formData = new FormData();
    formData.append('file', file);
    const header = new HttpHeaders();
    const params = new HttpParams();

    const options = {
      params,
      reportProgress: true,
      headers: header
    };
    const req = new HttpRequest('POST', this.url + this.getContollerName(controllerKey), formData, options);
    return this.http.request(req);
  }


  getContollerName(controllerKey: string) {
    return environment[controllerKey]; 
  }
}
