import { TestBed } from '@angular/core/testing';
import { UserInfo } from '../models/UserInfo';

import { SharedService } from './shared.service';

describe('SharedService', () => {
  let service: SharedService;

  beforeEach(() => {
    TestBed.configureTestingModule({ providers: [SharedService] });
    // TestBed.configureTestingModule({});
    service = TestBed.inject(SharedService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should be called set comment', () => {
    service.setComment("Sucess");
    // expect(service.setComment).toHaveBeenCalled();
  });

  it('should be called set filter', () => {
    service.setFilter(false);
    // expect(service.setFilter).toHaveBeenCalled();
  });

  it('should be called set user info details', () => {
    service.setUser(new UserInfo());
    // expect(service.setUser).toHaveBeenCalled();
  });
 
});
