import { HttpClient } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { of } from 'rxjs';

import { ApiService } from './api.service';

describe('ApiService', () => {
  let service: ApiService;
  let httpClientSpy: jasmine.SpyObj<HttpClient>;
  beforeEach(() => {
    const spy = jasmine.createSpyObj('HttpClient', ['get']);
    TestBed.configureTestingModule(
      {
        providers: [
          ApiService,
          {
            provide: HttpClient,
            useValue: spy
          }
        ]
      }
    );
    service = TestBed.inject(ApiService);
    httpClientSpy = TestBed.inject(HttpClient) as jasmine.SpyObj<HttpClient>;
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  /* #region  Create Mock method */
  it('Shall call get method', (done: DoneFn) => {
    const stubValue = {
      clientName: 'Mohan'
    };
    httpClientSpy.get.and.returnValue(of(stubValue));
    service.get("FeedBack").subscribe(res => {
      expect(res).toEqual(stubValue, 'expected heroes');
      done();
    },
      done.fail);
    expect(httpClientSpy.get.calls.count()).toBe(1, 'one call');
  });

  /* #endregion */

});
