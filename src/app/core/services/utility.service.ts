import { Injectable } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { SharedService } from './shared.service';

@Injectable({
  providedIn: 'root'
})
export class UtilityService {
  constructor(public domSanitizer  : DomSanitizer, public sharedService : SharedService) {}

  convertArrayToBase64SafeUrl(data : any, contentType : string) : any {
    let TYPED_ARRAY : any = new Uint8Array(data); 
    const STRING_CHAR = String.fromCharCode.apply(null, TYPED_ARRAY); 
    let base64String = btoa(STRING_CHAR); 
    return this.domSanitizer.bypassSecurityTrustUrl('data:'+contentType+';base64,' + base64String);
  }

  convertArrayToBase64(data : any, contentType : string) : any {
    let TYPED_ARRAY : any = new Uint8Array(data); 
    const STRING_CHAR = String.fromCharCode.apply(null, TYPED_ARRAY); 
    let base64String = btoa(STRING_CHAR); 
    return 'data:'+contentType+';base64,' + base64String;
  }

  converBase64ToArrayBuffer(base64 : string) {
    var binary_string = window.atob(base64);
    var len = binary_string.length;
    var bytes = new Uint8Array(len);
    for (var i = 0; i < len; i++) {
        bytes[i] = binary_string.charCodeAt(i);
    }
    return bytes;
}

  getMasterByLookUpName(lookUpName : string) {
    if(!this.sharedService.masters) {
      return [];
    }
    return this.sharedService.masters.filter((master: { lookUpName: string; }) => master.lookUpName === lookUpName)[0].lookupValues;
  }

  
   
}
