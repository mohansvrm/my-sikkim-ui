import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';


@Injectable({
    providedIn: 'root'
})
export class ConfigService {


    constructor() { }

    getRazoreConfig(): any {
        return {
            key: environment["RazorKey"],
            currency: environment["currency"]
        }
    }

}
