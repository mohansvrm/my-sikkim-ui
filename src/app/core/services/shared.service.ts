import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Alert } from '../models/Alert';
import { Header } from '../models/Header';
import { UserInfo } from '../models/UserInfo';

@Injectable({
  providedIn: 'root'
})
export class SharedService {
  public headerInfo : Header =  new Header();
  public userInfo : UserInfo = new UserInfo();
  public masters : any =[];
  constructor() { 
    
  }

  private user = new BehaviorSubject<UserInfo>(new UserInfo());
  castUser = this.user.asObservable();

  private comment = new BehaviorSubject<string>('');
  comment_Observable = this.user.asObservable();

  private filter = new BehaviorSubject<boolean>(false);
  filterEvent = this.filter.asObservable();

  private alert = new BehaviorSubject<Alert>(new Alert());
  triggerAlert = this.alert.asObservable();
   
   setUser(newUser : UserInfo){
     this.user.next(newUser); 
   }

   setFilter(acted : boolean) {
     this.filter.next(acted); 
   }
   
   setComment(value : string) {
    this.comment.next(value); 
  }

  setAlert(alert : Alert) {
    this.alert.next(alert);
  }
}
