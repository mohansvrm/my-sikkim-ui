import { TestBed } from '@angular/core/testing';

import { ConfigService } from './config.service';

describe('ConfigService', () => {
  let service: ConfigService;

  beforeEach(() => {
    TestBed.configureTestingModule({ providers: [ConfigService] });
    service = TestBed.inject(ConfigService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  /* #region  Just method check */
  it('should be created', () => {
    service.getRazoreConfig();
    // expect(service.getRazoreConfig).toHaveBeenCalled();
  });
  /* #endregion */

});
