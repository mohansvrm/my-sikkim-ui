import {Injectable} from '@angular/core';
import {SocialAuthService, SocialUser} from 'angularx-social-login';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {map, tap} from 'rxjs/operators';
import { SharedService } from './shared.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(private router: Router,
              private socialAuthService: SocialAuthService, private shareService : SharedService) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.socialAuthService.authState.pipe(
      map((socialUser: SocialUser) => !!socialUser),
      tap((isLoggedIn: boolean) => {
        if (!isLoggedIn) {
          this.router.navigate(['login']);
        } else {
          // if(this.shareService.userInfo.name) {
          //   this.shareService.userInfo.email = socialUser.email;
          //   this.shareService.userInfo.firstName = socialUser.firstName;
          //   this.shareService.userInfo.lastName = socialUser.lastName;
          //   this.shareService.userInfo.name = socialUser.name;
          //   this.shareService.userInfo.photoUrl = socialUser.photoUrl; 
          // }
        }
      })
    );
  }
}
