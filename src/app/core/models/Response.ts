export class Response {
    public errorCode : string = '';
    public errorMessage : string = '';
    public httpStatusCode : number = 200;
    public isSuccess : boolean = false;
    public data : any;
}