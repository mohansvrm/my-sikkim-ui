export class Order {
    public _id : any;
    public name : string = '';
    public orderId : string = '';
    public adultCount!: number;
    public adultFee!: number;
    public childrenCount!: number;
    public childrenFee!: number;
    public orderDate!: string;
    public userName!: string;
    public type !: string;
    public locationName  !: string;
    public razorPaymentId !: string;
}