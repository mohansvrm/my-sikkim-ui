import { Images } from "./Image";

export class Destination {
    public _id : any;
    public code: string = '';
    public destName: string = '';
    public address: string = '';
    public district: string = '';
    public isForeigner?: boolean ;
    public about?: string;
    public images : Images [] = [];
    public ticketBooking?: boolean ;
    public isPermitRequired?: boolean ;
    public permitFee : number = 0;
    public childrenFee: number = 0;
    public adultFee: number = 0;
    public isActive?: boolean ;
    public isDestination?: boolean = true ;
    public createdBy : string = '';
    public createdOn : Date | undefined;
    public modifiedBy : string = '';
    public modifiedOn : Date | undefined;

}

//  export class Fees {
//      public type : string = '';
//      public fee : number = 0;
//  }