export class UserInfo {
    public _id : any;
    public emailId : string = '';
    public userName : string = ''; 
    public userType : any = '';
    public fullName : string = ''; 
    public password : string = ''; 
    public phoneNumber : string = '';  
    public address : string = '';
    public profile :   string= '';
    public vaccine :   string= '';
    public idproof :   string= '';
    public role :   string= '';
    public otp :   string= '';
    public gAuth :   boolean= false;
    public isActive : boolean = true;
    public isTempPwd : boolean = true;
}