export class Trek {
    public _id : any;
    public hotelName: string = '';
    public hotelAddress: string = '';
    public district?: string;
    public about?: string;
    public rating?: string;
    public overview?: string;
    public phoneNoOne?: string;
    public phoneNoTwo?: string;
    public mailIdOne?: string;
    public mailIdTwo?: string;
    public images : TrekImages [] = [];
}

export class TrekImages {
    public isMainImage: boolean = false;
    public path: string = '';
    constructor(isMainImage : boolean, path : string) {
        this.isMainImage = isMainImage;
        this.path = path;
    }
}