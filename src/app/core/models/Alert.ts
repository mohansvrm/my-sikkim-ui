export class Alert {
    public type : string = '';
    public message : string = '';
    public isClosed : boolean = true; 

    constructor(type : string = '',message : string = '',isClosed : boolean = true) {
        this.type = type;
        this.message = message;
        this.isClosed = isClosed;
    }
}