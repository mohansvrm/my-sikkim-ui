export class Images {
    public isMainImage: boolean = false;
    public path: string = '';
    constructor(isMainImage : boolean, path : string) {
        this.isMainImage = isMainImage;
        this.path = path;
    }
}