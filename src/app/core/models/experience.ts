export class Experience {
    public experienceType: string = '';
    public images : ExperienceImages [] = [];
    public contentType?: string;
    public about?: string;
    public title: string = '';
}

export class ExperienceImages {
    public isMainImage: boolean = false;
    public path: string = '';
    constructor(isMainImage : boolean, path : string) {
        this.isMainImage = isMainImage;
        this.path = path;
    }
}