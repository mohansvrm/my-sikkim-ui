import { Images } from "./Image";

export class Hotel {
    public _id : any;
    public hotelName: string = '';
    public hotelAddress: string = '';
    public district?: string;
    public about?: string;
    public rating?: string;
    public overview?: string;
    public phoneNoOne?: string;
    public phoneNoTwo?: string;
    public mailIdOne?: string;
    public mailIdTwo?: string;
    public isActive?:boolean;
    public images : Images [] = [];
    public documents: HotelDocuments[] = []; 
    public amenities: any[] = []; 
    public hotelOwner : string = '';
    public status : string = 'Initiated';
    public comments : any [] = [];
}



export class HotelDocuments {
    public documentName: string = '';
    public path: string = '';
    constructor(documentName : string, path : string) {
        this.documentName = documentName;
        this.path = path;
    }
}
export class Amenities {
    public name: string = '';
    public image: string = '';
    constructor(name : string, image : string) {
        this.name = name;
        this.image = image;
    }
}