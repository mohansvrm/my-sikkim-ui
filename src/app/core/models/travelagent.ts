export class Travelagent {
  
    public _id : any;
    public code: string = '';
    public agentName: string = '';
    public address: string = '';
    public grade?: string
    public phoneNumber?: string;
    public emailid?: string;
    public amenities: any[] = []; 
    public status : string = 'Initiated';
    public comments : any [] = [];
}
