export class Header {
    public logo: boolean = true;
    public navBtn: boolean = true;
    public backBtn : boolean = false;
    public backUrl : string = '';
    public heading : boolean = false;
    public headingText : string = '';
    public filterBtn : boolean = false;
    public filters : any;
    public cartIcon : boolean = false;
    public cartCount : any;
}

