import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-login-option',
  templateUrl: './login-option.component.html',
  styleUrls: ['./login-option.component.css']
})
export class LoginOptionComponent implements OnInit {
  type : any;
  constructor(private _Activatedroute:ActivatedRoute) {
    
   }

  ngOnInit(): void {
    this.type=this._Activatedroute.snapshot.paramMap.get("type");
  }

}
