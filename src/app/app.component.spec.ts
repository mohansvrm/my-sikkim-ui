import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { ApiService } from './core/services/api.service';
import { LoaderService } from './core/services/loader.service';
import { SharedService } from './core/services/shared.service';

describe('AppComponent', () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  let loaderServiceStub : Partial<LoaderService>;
  let sharedServiceStub : Partial<SharedService>;
  let apiServiceStub : Partial<ApiService>;
  let loaderService : LoaderService;
  let sharedService : SharedService;
  let apiService : ApiService;
    beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
       
        AppComponent
      ],
      providers : [
        { provide: ApiService, useValue: apiServiceStub },
        LoaderService,
        SharedService
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    loaderService = fixture.debugElement.injector.get(LoaderService);
    sharedService = fixture.debugElement.injector.get(SharedService);
    apiService = fixture.debugElement.injector.get(ApiService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
 
});
