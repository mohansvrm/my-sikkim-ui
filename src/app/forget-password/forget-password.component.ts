import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { InputPattern } from '../core/pattern/InputPattern';
import { ApiService } from '../core/services/api.service';
import { SharedService } from '../core/services/shared.service';

@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.css']
})
export class ForgetPasswordComponent implements OnInit {
  emailId : string = '';
  errorMessage: string = '';
  userType : any = '';
  emailPattern : string = InputPattern.EMAIL;
  constructor(public sharedService : SharedService,public apiService: ApiService,
    private router: Router,private _Activatedroute:ActivatedRoute) { 
      this.userType=this._Activatedroute.snapshot.paramMap.get("type");
    }

  ngOnInit(): void {
  }

  onClickSendOtp() {
    this.errorMessage = '';
    this.apiService.put("UserReqOtp","/" + this.userType+"/" + this.emailId,null).subscribe(res => {
      console.log(res);
      if(res.isSuccess) {
        this.sharedService.setUser(res.data);
        this.router.navigate(['/reset-password','otp']);
      } else {
        this.errorMessage = res.data;
      }
    })
  }

}
