import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { LoginOptionComponent } from './login-option/login-option.component';
import { UserRegisterComponent } from './user-register/user-register.component';
import { CommentsPopupComponent } from './home/comments-popup/comments-popup.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { ForgetPasswordComponent } from './forget-password/forget-password.component';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then(m => m.HomeModule)
  },

  { path: 'login/:type', component: LoginComponent },
  { path: 'comments-popup', component: CommentsPopupComponent },
  { path: 'login-options', component: LoginOptionComponent   },
  { path: 'user-register/:type', component: UserRegisterComponent   },
  { path: 'reset-password/:type', component: ResetPasswordComponent   },
  { path: 'forget-password/:type', component: ForgetPasswordComponent   },
  { path: '', redirectTo: '/login-options', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
