import { Component, OnInit } from '@angular/core';
import { Alert } from '../core/models/Alert';
import { UserInfo } from '../core/models/UserInfo';
import { InputPattern } from '../core/pattern/InputPattern';
import { ApiService } from '../core/services/api.service';
import { SharedService } from '../core/services/shared.service';
import {Response } from '../core/models/Response';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {
  userInfo : UserInfo = new UserInfo();
  pwdPattern : string = InputPattern.PASSWORD; 
  tempPassword : string = '';
  otPassword : string = '';
  newPassword : string = '';
  confirmNewPassword : string = '';
  resetType: any = '';
  constructor(public sharedService : SharedService,public apiService: ApiService,
    private router: Router,private _Activatedroute:ActivatedRoute) {
    this.sharedService.castUser.subscribe(user => this.userInfo = user);
    this.resetType=this._Activatedroute.snapshot.paramMap.get("type");
   }

  ngOnInit(): void {
  }

  onClickOfReset() {
    if(this.newPassword !== this.confirmNewPassword) {
      alert('Password and Confirm Password is mismatch');
      return;
     }
     if(this.resetType === 'otp') {
      if(this.userInfo.otp !== this.otPassword) {
        alert('OTP is wrong');
         return;
       }
     } else {
      if(this.userInfo.password !== this.tempPassword) {
        alert('Temporary password is mismatch');
         return;
       }
     }
    

     this.userInfo.password = this.newPassword;
     this.userInfo.isTempPwd = false;

     this.apiService.put('User','/'+this.userInfo._id, this.userInfo).subscribe((result: Response) => {
      if (result.isSuccess) {
          this.sharedService.setUser(result.data);
          this.router.navigate(['/home']);
      }
    });


  }

}
